# Manuale utente
## A) Installazione
-  Prerequisiti: Java (JDK 11 o superiore) ed Eclipse installati nel computer.
-  Per l'installazione del nostro progetto tramite Bitbucket, il primo passo consiste nell'importare in Eclipse il link ottenuto premendo il bottone 'Clone' presente in questa pagina: [Repository Bitbucket](https://bitbucket.org/tfede/sweng/src/master/)
-  Una volta mandato in esecuzione Eclipse cliccare su File/Import
![Import1](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609f95126810672ab77fac35/6fc87395d447d872a755349be2887b0b/001_ImportProject.JPG)
- Cliccare su Git/Project from Git
![Import2](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609f95126810672ab77fac35/df4a98192b8cd021952318351b8466fb/002_ImportProject.JPG)
- Selezionare Clone URI
![Import3](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609f95126810672ab77fac35/1d34b454b8e4fd14e2f3d021d0943fcd/003_ImportProject.JPG)
- Cliccare su Next
![Import4](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609f95126810672ab77fac35/6dc224ac3a8bb580f184ab22dc17b2a3/004_ImportProject.JPG)
- Cliccare su Next assicurandosi di aver selezionato il branch master
![Import5](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609f95126810672ab77fac35/9821c0427ab226cd469c1b009134ac35/005_ImportProject.JPG)
- Selezionare la directory in cui salvare il progetto e cliccare su Next
![Import6](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609f95126810672ab77fac35/94bd3c315d429b5dd66074c2a130c08e/006_ImportProject.JPG)
- Selezionare import Eclipse existing project e cliccare su Next
![Import7](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609f95126810672ab77fac35/6b263769137cfad1ef80269bbe6d9f1e/007_ImportProject.JPG)
- Cliccare su Finish
![Import8](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609f95126810672ab77fac35/ccd017186dab7ae889b5e510d48a0bbc/008_ImportProject.JPG)
-  Successivamente il file da runnare è IlRestoDelBolognino.java all'interno della cartella "/IlRestoDelBolognino/src/bolognino/client/IlRestoDelBolognino.java", da eseguire con “GWT Development mode with Jetty”.
## B) Utilizzo e scelte implementative

All'avvio dell'applicazione web (da eseguire come specificato nel punto A), vi si presenterà una schermata, nella quale vi è la possibilità di iscriversi o loggarsi come utente della 
piattaforma o di registrarsi come autore e le preview degli articoli presenti sul sito.
Questa schermata iniziale è stata implementata con l'ausilio di una classe "HomePage.java", la quale non è collegata al concetto di User, dal momento in cui in questa prima fase un utente non risulta ancora loggato, di conseguenza non si sa la tipologia di utente che sta richiedendo la pagina.
Trattando l'utente come ospite sono state realizzate anche le seguenti pagine/classi:

- **AccessRegistrationAuthorPage.java**: pagina che viene invocata successivamente la pressione del pulsante nella Home "Registrati come scrittore", che reindirizza l'utente alla pagina di inserimento token (una chiave segreta che viene fornita dal giornale), per permettere all'autore di registrarsi come tale;

- **RegistrationAuthorPage.java**: pagina che viene invocata successivamente il passaggio nella pagina "AccessRegistrationAuthorPage.java", se il token inserito risulta corretto (Token: “123STELLA”). Questa pagina permette ad un autore di registrarsi;

- **RegistrationUserPage.java**: pagina che viene invocata successivamente la pressione del pulsante nella Home "Registrati come utente", che reindirizza l'utente alla pagina di registrazione dello stesso

- **ArticlePage.java**: pagina che viene invocata successivamente la pressione di un titolo di un Article (non ArticlePremium), che reindirizza l'utente alla visualizzazione dell'articolo, nella quale non essendo loggato non potrà né commentare né inserire like.                        Questo controllo, così come tutti i controlli durante la pressione dei vari bottoni "post login", viene effettuato mediante l'ausilio di un cookie di sessione;
- **LoginPage.java**: pagina che viene invocata successivamente la pressione del pulsante nella Home "Login", per dare la possibilità a tutti gli utenti di loggarsi mediante le proprie credenziali.


Successivamente, dopo aver effettuato il login si può accedere ad una "seconda categoria" di pagine, ovvero quelle specifiche all'utente loggato, il quale può essere di 4 tipologie:

- Admin;
- Author;
- Subscribed;
- User.
#
Per ciascuno di questi utenti è stata fatta la scelta implementativa di far visualizzare una pagina differente, sfruttando così uno dei principi fondamentali della programmazione ad oggetti affrontato durante il corso, ovvero l'ereditarietà.
Mediante l'Override delle varie pagine siamo riusciti a caratterizzare le azioni conseguenti all'interazione con gli utenti in base alla tipologia degli stessi.
Quindi dal punto di vista tecnico, le chiamate si vanno ad effettuare non più come ospite (quindi con le classi distinte viste in precedenza), bensì come utente loggato, quindi richiedendo la funzione espressamente descritta nell'utente stesso.

#### L’utente registrato (User) ha la possibilità di :
- Visualizzare i dati immessi nella fase di registrazione;
- Visualizzare e consultare tutti gli articoli free che il sito mette a disposizione;
- Commentare un articolo free;
- Mettere mi piace ad un articolo free;
- Visualizzare la pagina profilo di un autore;
- Visualizzare le proprie interazioni con il sito (commenti effettuati, mi piace messi);
- Abbonarsi al sito.

#### Grazie all’ereditarietà, l’utente abbonato (Subscribed) ha la possibilità di fare le stesse azioni di un utente con l’aggiunta di:
- Visionare i dati di sottoscrizione dell’abbonamento
- Visionare tutti gli articoli premium che il sito mette a disposizione;
- Commentare un articolo premium;
- Mettere mi piace ad un articolo premium;
- Disdire l’abbonamento;

#### L’utente scrittore (Author) ha la possibilità di:
- Visualizzare tutti gli articoli sia free che premium che il sito mette a disposizione;
- Mettere mi piace ad un articolo che non sia di sua proprietà;
- Commentare un articolo che non sia di sua proprietà;
- Creare un articolo;
- Modificare un suo articolo;
- Cancellare un suo articolo;

#### L’utente amministratore (Admin) ha la possibilità di:
- Visualizzare tutti gli articoli sia free che premium che il sito mette a disposizione;
- Gestire le categorie e le rispettive sottocategorie (inserimento, modifica e rimozione);
- Moderare i commenti pubblicati dagli utenti registrati;
- Creare un articolo a nome di un autore;
- Modificare un articolo pubblicato sul sito;
- Eliminare un articolo pubblicato sul sito;
- Eliminare un commento di un articolo, moderato precedentemente.

#### Abbiamo suddiviso i servizi in quattro classi:
- **UserServiceImpl**: servizi messi a disposizione per l’utente (User e Subscribed);
- **AuthorServiceImpl**: servizi messi a disposizione per l’utente autore;
- **AdminServiceImpl**: servizi messi a disposizione per l’utente amministratore;
- **MySessionServiceImpl**: servizi per la gestione della sessione degli utenti.

#### Inoltre, abbiamo fatto uso di tre Mappe di MapDB per la persistenza dei dati:
- **Article_Table**, una Map<Integer, Article> per la memorizzazione degli articoli;
- **User_Table**, una Map<Integer, User> per la memorizzazione degli utenti;
- **Categories_Table**, una Map<String, ArrayList<String>> per la memorizzazione delle categorie e le rispettive sottocategorie.

# Scelte progettuali e di interfaccia
## Interfacce User & Subscribed
Per la realizzazione del nostro progetto abbiamo sviluppato un’interfaccia utente semplice e intuitiva che garantisca un rapporto user friendly con il nostro applicativo.

Di fatto, una volta che ci si connette al sito, si viene indirizzati alla HomePage generale (Figura 1.0) da cui è possibile avere una panoramica degli articoli pubblicati sul sito.

![N|Solid](https://trello-attachments.s3.amazonaws.com/609e8b0b6af55549abe4de12/1200x498/3cd216a3956137cf8f01cd06cb810ec5/00_Avvio.JPG.jpg)
**Figura 1.0**
#
Come si può notare dalla figura, è possibile visionare i titoli dei singoli articoli e una preview del loro contenuto. Se a questo punto si tenta di accedere agli articoli cliccando sul titolo di questi, nel caso di articoli free viene aperta la pagina dedicata dell’articolo (Figura 2.0) dove è possibile consultare il contenuto e gli eventuali commenti.
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/eba55617a8951eb3bc3c2699a948fc87/13_ArticlePage.JPG)
**Figura 2.0**
#
Dal momento che non si è ancora effettuata l’autenticazione alla piattaforma non risulterà possibile ‘mettere like’ o commentare l’articolo, nel caso l’utente provasse a compiere una delle due azioni appena citate viene reindirizzato all’interfaccia di registrazione al sito.

Nel caso l’utente desiderasse di fatto registrarsi è possibile effettuarlo cliccando sul bottone ‘Registrati come utente’ presente nella HomePage e aprendo così il form di registrazione (Figura 3.0)

![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/f5fc850fb5e8e338d88ef536e82f040a/01_01_RegistrazioneUser.JPG)
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/106f14756c6ff9b976c9de0c775a86a9/01_02_RegistrazioneUser.JPG) 
**Figura 3.0**
#
Come riportato da documento di specifica, all’utente viene chiesto di compilare tutti i campi obbligatori* e eventualmente i campi facoltativi tra cui la scelta delle categorie di notizie che più gli interessano per garantirgli così una migliore esperienza di navigazione.

In alternativa al form di registrazione per utenti non ancora iscritti, se l’utente possiede già un account, è possibile effettuare l’autenticazione attraverso l’apposito form di login (Figura 4.0).
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/aa8231b950003ac3e623b7b8a9c634af/04_Login.JPG)
**Figura 4.0**
#
A questo punto, una volta autenticati al sito, l’utente accede alla propria HomePage dedicata in cui viene data la possibilità di scegliere se consultare gli articoli appartenenti alle categorie preferite selezionate in fase di registrazione (Figura 5.0) o di poter visualizzare tutte le ultime notizie pubblicate sul giornale (Figura 5.1).
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/a7a4a7d4382694239d320dd5af30dfa5/05_HomePageUser_NewsFiltred.JPG)
**Figura 5.0**
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/4f5f9e69bf2ae2d9a1ce8007c7c83fd8/05_HomePageUser_AllNews.JPG)
**Figura 5.1**
#
A questo punto l’utente ha la possibilità di accedere alla sua ProfilePage cliccando sul bottone ‘Visualizza il tuo profilo’ in cui potrà osservare la lista delle sua attività all’interno del sito (Like e Commenti) (Figura 6.0).
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/bc08f98ad81128efb761be549e8fa824/06_MainPageUser.JPG)
**Figura 6.0**
#
Dalla sua ProfilePage, l’utente può visualizzare i propri dati inseriti in fase di registrazione (Figura 7.0).
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/21301fbc772603b1c3303cd356265fa2/07_DatiUser.JPG)
**Figura 7.0**
#
Tornando alla propria ProfilePage(Figura 6.0) cliccando su ‘Torna al profilo’, in alternativa alla consultazione dei propri dati personali è possibile sottoscrivere un abbonamento al giornale, garantendosi in questo modo l’accesso ai contenuti premium, cliccando su ‘Abbonati al giornale’ e venendo così reindirizzati alla pagina di sottoscrizione di un abbonamento (Figura 8.0).

![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/d7334c0ddfcd913583a19f4dbb66db1d/08_InserimentoDatiCarta.JPG)
**Figura 8.0**
#
Di fatto, compilando tutti i campi richiesti, questi vengono memorizzati e associati al proprio profilo utente passando così da utente registrato a utente abbonato modificando di conseguenza la propria ProfilePage (9.0) con l’inserimento dei bottoni ‘Visualizza dati abbonamento’ tramite il quale è possibile consultare le informazioni relative ad esso (Figura 9.1) e ‘Disdici l’abbonamento al giornale’ per mezzo del quale è possibile eliminare i propri dati di pagamento passando così da utente abbonato a utente registrato.
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/391559db7a18748dbcfb5be18045cffb/20_ButtonSubscribed.JPG)
**Figura 9.0**
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/7ac5999c0cca214188a58ad6962e4cac/09_DatiCarta.JPG)
**Figura 9.1**
#
Tornando alla consultazione articoli, quindi alla HomePage utente, si ha quindi la possibilità di accedere agli articoli di tipo free e di interagire con essi (like e commenti) sia come utente registrato che abbonato. Per gli utenti abbonati è possibile effettuare le medesime azioni con gli articoli di tipo premium, mentre se un utente unicamente registrato tentasse di accedere a un contenuto premium verrebbe indirizzato alla pagina di sottoscrizione abbonamento. (Figura 8.0).

## Interfacce Author
Ripartendo dalla HomePage generale (Figura 1.0) passiamo ora a ciò che riguarda gli utenti di tipo autore.
Di fatto se questi non fossero ancora registrati al sito, è possibile farlo cliccando sul bottone ‘Registrati come scrittore’ ma, prima di passare al form di registrazione, dato che un autore gode degli stessi diritti di un utente abbonato ma senza necessità di pagamento, bisogna prima effettuare un’autenticazione tramite token [123STELLA] (Figura 10.0).
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/854b317395fbc4ef9737c9b7fcdabcbd/02_TokenRegistrAutore.JPG)
**Figura 10.0**
#
Passato questo step si passa al form di registrazione che si presenta in maniera analoga a quello dell’utente (Figura 10.1).
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/997eb475647a7052986f0c31074cfa99/03_01_RegistrazioneAuthor.JPG)
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/433ac3e3a10b1cd4a58f2563be75e455/03_02_RegistrazioneAuthor.JPG)
**Figura 10.1**
#
Una volta effettuata la registrazione o l’autenticazione (Figura 4.0) si accede alla HomePage dell’autore (Figura 11.0) dove risulta possibile consultare gli ultimi articoli caricati sul giornale.
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/ff19262f0bb098d68739072652cd7f07/10_HomePageAuthor.JPG)
**Figura 11.0**
#
A questo punto l’autore può accedere alla propria ProfilePage (Figura 12.0) da cui potrà consultare tutti gli articoli che ha scritto.
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/8363ac439aeb4843dd35d69332acd9e1/11_ProfilePageAuthor.JPG)
**Figura 12.0**
#
Tornando nuovamente alla HomePage e cliccando su ‘Crea un articolo’ si accede al form di creazione degli articoli (Figura 13.0) da cui è di fatto possibile pubblicarne uno compilando tutti i campi obbligatori* che lo compongono.
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/fb7d0f1196c8b5f1558135340657680c/12_FormCreaArticoloAuthor.JPG)
**Figura 13.0**
#
L’autore può accedere ad un suo articolo (Figura 14.0) , con la possibilità in primo luogo di consultarlo e con l’aggiunta di due azioni: modifica e/o rimozione dello stesso.
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/e76c5e94bc4706765a600959604aa4d1/19_ModifyArticleAuthor.JPG)
**Figura 14.0**
#
## Interfacce Admin
L’amministratore di sistema alias Admin accede attraverso il form di autenticazione (Figura 4.0) e una volta autenticato accede alla propria HomePage (Figura 15.0).

![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/c02d4b782b566cc66dcf9c53172e3b32/14_HomePageAdmin.JPG)
**Figura 15.0**
#
Essendo a tutti gli effetti un ‘super-user’ l’Admin ha la possibilità di consultare liberamente gli articoli di ogni tipo.
In quanto amministratore di sistema ha la possibilità di gestire: Categorie articoli e relative Sottocategorie, Commenti e Articoli. 
Cliccando su ‘Vai alla pagina gestionale’ si viene rimandati alla pagina da cui è possibile accedere alle diverse mansioni dedicate all’Admin (Figura 16.0).
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/b181730e37e08405bb084cb15b2ae65b/15_ManagePageAdmin.JPG)
**Figura 16.0**
#
Cliccando su ‘Gestisci categorie’ viene aperto il form riguardante queste ultime e le rispettive sottocategorie (Figura 16.1), dando la possibilità all’Admin di aggiungerne, modificarne ed eliminarle.
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/9742ebe65044ef3592ad3d273c9a900a/16_ManageCategoryPage.JPG)
**Figura 16.1**
#
Cliccando invece su ‘Moderazione commenti’ viene aperto il form da cui l’Admin ha la possibilità di visionare tutti i commenti, da moderare, agli articoli del sito (Figura 16.2) e decidere se approvarli o respingerli nel caso non risultassero adeguati.
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/7ab45d3b4d2709e5882a2ba5f4bf11ca/17_ManageCommentPage.JPG)
**Figura 16.2**
#
Cliccando infine sul bottone ‘Crea articolo’ viene aperto un form del tutto analogo a quello riguardante l’autore e la creazione articoli (Figura 16.3), con l’unica differenza che, dal momento che l’Admin non può inserire articoli a proprio nome, prima di compilare i campi necessari per produrre l’articolo è necessario che scelga l’autore a nome del quale pubblicarlo.
![N|Solid](https://trello-attachments.s3.amazonaws.com/60794d363a166c392916f4be/609e8b0b6af55549abe4de12/c5b4ec0727bc6c1044c34c667fe3d3fc/18_CreateArticleByAdmin.JPG)
**Figura 16.3**

# Descrizione del metodo
#### Iterazioni
Premessa l’ampia flessibilità che ci siamo concessi essendo solamente in tre, solitamente per organizzare al meglio il lavoro nelle varie fasi, abbiamo adottato una metodologia, la quale consiste in 3 punti:
- Il primo passo per capire come sviluppare il codice è la fase di analisi del lavoro da svolgere, coincidente con la pianificazione dello sprint. Questa fase è stata essenziale per capire al meglio come suddividere il lavoro;
- Successivamente ha inizio la fase di sviluppo;
- Il processo si conclude con la fase di testing.

Questa metodologia iterativa l’abbiamo mantenuta per tutti gli sprint.

#### Milestone
Le milestone presuppongono una data di scadenza, o comunque sia il compimento di determinate specifiche/versioni del progetto. L’unica milestone vera e propria da noi identificata risulta la consegna dell’elaborato, dal momento in cui essendoci tenuti sempre in stretto contatto durante le varie fasi di progettazione, fra di noi ci sono sempre stati continui aggiornamenti, senza alcuna necessità di definire delle vere e proprie milestone.

#### Issue
Popolando il product backlog abbiamo cercato di stimare al meglio le issue, quindi principalmente i task da compiere, cercando di farle quanto più dettagliate possibili. La nostra principale metodologia per cercare di definire i task è stata una naturale conseguenza della fase di analisi dei requisiti, dal momento in cui all’inizio del progetto abbiamo fatto vari colloqui e discusso assieme in merito al lavoro da svolgere, anche durante la fase di progettazione (seppur rudimentale inizialmente) dei vari artefatti (più nello specifico dei diagrammi).

#### Assegnazione di priorità ai task
Volutamente non abbiamo assegnato alcuna priorità ai task, se non per l’installazione del software necessario, task ritenuto di massima priorità per iniziare a svolgere il progetto, dal momento in cui non sapevamo quanto tempo ci volesse per effettuare i vari task, tutti essenziali per rispettare le specifiche assegnate. Questa assegnazione paritaria di priorità a tutti i task è stata funzionale, dal momento in cui come sottolineato nei punti precedenti, per procedere al meglio con lo sviluppo c’è stata una quanto più ampia cooperazione al fine di risolvere i problemi sorti nel corso delle varie fasi di programmazione, quanto più velocemente possibile.
