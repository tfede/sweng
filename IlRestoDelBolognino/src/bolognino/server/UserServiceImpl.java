package bolognino.server;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletContext;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import bolognino.client.AccessRegistrationAuthorPage;
import bolognino.client.Admin;
import bolognino.client.Article;
import bolognino.client.HomePage;
import bolognino.client.LoginPage;
import bolognino.client.RegistrationUserPage;
import bolognino.client.Subscribed;
import bolognino.client.RegistrationAuthorPage;
import bolognino.client.User;
import bolognino.client.Author;
import bolognino.client.Category;
import bolognino.client.Comment;
import bolognino.client.UserService;

public class UserServiceImpl extends RemoteServiceServlet implements UserService {
	/*Metodo che ritorna la pagina di registrazione dell'utente*/
	public RegistrationUserPage getRegistrationUserPage() {
		return new RegistrationUserPage();
	}
	/*Metodo che ritorna la pagina di registrazione dell'autore*/
	public RegistrationAuthorPage getRegistrationAuthorPage() {
		return new RegistrationAuthorPage();
	}
	/*Metodo che ritorna la pagina di immissione del token, prima di accedere alla registrazione dell'autore*/
	public AccessRegistrationAuthorPage getAccessRegistrationAuthorPage() {
		return new AccessRegistrationAuthorPage();
	}
	/*Metodo che ritorna la prima pagina del sito agli utente non loggati*/
	public HomePage getHomePage() {
		return new HomePage();
	}
	/*Metodo che ritorna la pagina di login*/
	public LoginPage getLoginPage() {
		return new LoginPage();
	}
	/*Metodo che consente di registrare un nuovo utente con la memorizzazione nella mappa di utenti (Map<Integer,User>)*/
	public Boolean newUser(String nickname, String password, String email, String firstname, String surname, Date birthday, String state, String province, String region, String city, String address, Boolean gender, String birthPlace, ArrayList<String> categories, ArrayList<String> likesAndComments) {
		DB db = getTable();
		Map<Integer, User> map = db.getTreeMap("user");

		User user = new User(nickname, password, email, firstname, surname, birthday, state, province, region, city, address, gender, birthPlace, categories, likesAndComments);
		/*Controllo sull'unicit� del NICKNAME e dell'EMAIL*/
		if(uniqueNickame(map,user.getNickname()) && uniqueEmail(map,user.getEmail())){
			if(map.size() == 0) {
				map.put(map.size(), user); 
			}else {
				map.put(Collections.max(map.keySet()) + 1, user); 
			}
			db.commit();
			return true;
		}
		return false;
	}
	/*Metodo che rende un utente da User a Subscribed (da utente non abbonato a utente abbonato)*/
	public User upgradeUser(User user, String ncc, String cardHolderName, String cardHolderSurname, Date cardDeadline, String cvv, String nation, String cap) {
		DB db = getTable();
		Map<Integer, User> map = db.getTreeMap("user");
		if(map.entrySet().stream().filter(x -> user.getNickname().equals(x.getValue().getNickname())).findFirst().isPresent()) {
			for (Map.Entry<Integer, User> entry : map.entrySet()) {
				/*Ricerca dell'entry nella Map dell'utente*/
			    if(entry.getValue().getNickname().equals(user.getNickname())) {
			    	User subscribed = new Subscribed(user.getNickname(),user.getPassword(),user.getEmail(),user.getFirstname(),user.getSurname(),user.getBirthday(),user.getState(),user.getProvince(),user.getRegion(),user.getCity(),user.getAddress(),user.getGender(),user.getBirthPlace(),user.getCategories(),user.getLikesAndComments(),ncc, cardHolderName, cardHolderSurname, cardDeadline, cvv, nation, cap);
			    	/*Rimpiazzamento dell'entry con il "nuovo utente" Subscribed*/
			    	map.put(entry.getKey(), subscribed);
					db.commit();

					return subscribed;
			    }
			}
		}
		return null;
	}
	/*Metodo che rende un utente da Subscribed a User (da utente abbonato a utente non abbonato*/
	public User downgradeUser(User user) {
		DB db = getTable();
		Map<Integer, User> map = db.getTreeMap("user");
		if(map.entrySet().stream().filter(x -> user.getNickname().equals(x.getValue().getNickname())).findFirst().isPresent()) {
			for (Map.Entry<Integer, User> entry : map.entrySet()) {
				/*Ricerca dell'entry nella Map dell'utente*/
			    if(entry.getValue().getNickname().equals(user.getNickname())) {
			    	User userNew = new User(user.getNickname(),user.getPassword(),user.getEmail(),user.getFirstname(),user.getSurname(),user.getBirthday(),user.getState(),user.getProvince(),user.getRegion(),user.getCity(),user.getAddress(),user.getGender(),user.getBirthPlace(),user.getCategories(),user.getLikesAndComments());
			    	/*Rimpiazzamento dell'entry con il "nuovo utente" User*/
				    map.put(entry.getKey(), userNew);
					db.commit();
					return userNew;
			    }
			}
		}
		return null;
	}
	/*Metodo che restituisce tramite NICKNAME l'utente in questione*/
	public User getUserByNickname(String nicknameUser) {
		DB db = getTable();
		Map<Integer, User> map = db.getTreeMap("user");		
		
		for (Map.Entry<Integer, User> entry : map.entrySet()) {
			/*Ricerca dell'entry in Map del relativo utente*/
			if(entry.getValue().getNickname().equals(nicknameUser)) {
				return entry.getValue();
			}
		}
		return null;
	}
	/*Metodo che consente di registrare un nuovo autore con la memorizzazione nella mappa di utenti (Map<Integer,User>)*/
	public Boolean newAuthor(String nickname, String password, String email, String firstname, String surname, Date birthday, String state, String province, String region, String city, String address, Boolean gender, String birthPlace, ArrayList<String> categories, ArrayList<String> likesAndComments) {
		DB db = getTable();
		Map<Integer, User> map = db.getTreeMap("user");
		User user = new Author(nickname, password, email, firstname, surname, birthday, state, province, region, city, address, gender, birthPlace, categories, likesAndComments);
		/*Controllo di unicit� di NICKNAME e EMAIL*/
		if(uniqueNickame(map,user.getNickname()) && uniqueEmail(map,user.getEmail())){
			if(map.size() == 0) {
				map.put(map.size(), user); 
			}else {
				map.put(Collections.max(map.keySet()) + 1, user); 
			}
			//Inseriamo l'utente solo se la e-mail e il nickname sono unici
			db.commit();
			return true;
		}
		return false;
	}

	/*public Boolean newAdmin() {
		DB db = getTable();
		Map<Integer, User> map = db.getTreeMap("user");

		User user = new Admin("admin", "admin");
		if(uniqueNickame(map,user.getNickname())) {
			map.put(map.size(), user); //Inseriamo l'admin soltanto una volta
			db.commit();
			return true;
		}
		return false;
	}*/
	/*Metodo per loggarsi al sito*/
	public User getAuthentication(String nickname, String password)  {
		DB db = getTable();
		Map<Integer, User> map = db.getTreeMap("user");
		for (Map.Entry<Integer, User> entry : map.entrySet()) {
			if(entry.getValue().getNickname().equals(nickname) && entry.getValue().getPassword().equals(password)) {
				//Qui sei loggato
				return entry.getValue();
			}
	    }
		return null;
	}
	/*Metodo per regolarizzare gli accessi alla registrazione degli autori. 
	 * Abbiamo inserito un token di accesso, cos� da evitare che tutti gli utenti possano registrarsi come autori.*/
	public Boolean checkAuthenticationToken(String token) {
		if(token.equals("123STELLA")) {
			return true;
		}
		return false;
	}
	/*Metodo che restituisce la MAPPA (tabella) degli Utenti */
	private DB getTable() {
		ServletContext context = this.getServletContext();
		synchronized (context) {
			DB user_table = (DB)context.getAttribute("USER_TABLE");
			if(user_table == null) {
				user_table = DBMaker.newFileDB(new File("user_table")).closeOnJvmShutdown().make();
				context.setAttribute("USER_TABLE", user_table);
				
				Map<Integer,User> users = user_table.getTreeMap("user");
				/*Se la tabella degli utenti � vuota viene riempita con un unico utente, ovvero l'Admin con NICKNAME admin e PASSWORD admin*/
				if(users.size() == 0) {
					users.put(users.size(),new Admin("admin","admin"));
					user_table.commit();
				}
				
			}
			return user_table;
		}
	}
	/*Metodo che controlla l'unicit� del nickname nella mappa degli utenti*/
	public boolean uniqueNickame(Map<Integer,User> map, String nickname){
		if(map.entrySet().stream().filter(x -> nickname.equals(x.getValue().getNickname())).findFirst().isPresent())
			return false;
		return true; 		// inserimento da effettuare
	}
	/*Metodo che controlla l'unicit� dell'email nella mappa degli utenti*/
	public boolean uniqueEmail(Map<Integer,User> map, String email){
		if(map.entrySet().stream().filter(x -> email.equals(x.getValue().getEmail())).findFirst().isPresent())
			return false;
		return true; 		// inserimento da effettuare
	}
	/*Metodo che restituisce la MAPPA (tabella) delle Categorie*/
	private DB getCategories() {
		ServletContext context = this.getServletContext();

		synchronized (context) {
			DB categories_table = (DB)context.getAttribute("CATEGORIES_TABLE");
			if(categories_table == null) {	//se non esiste la table crea quelle di default, altrimenti restituisce la table
				categories_table = DBMaker.newFileDB(new File("categories_table")).closeOnJvmShutdown().make();
				context.setAttribute("CATEGORIES_TABLE", categories_table);
				
				Map<String,ArrayList<String>> categories = categories_table.getTreeMap("category");
				/*Se la tabella delle categorie � vuota viene riempita con le 10 categorie di default*/
				if(categories.size() == 0) {
					categories.put("Italia",new ArrayList<String>());
					categories.put("Mondo",new ArrayList<String>());
					categories.put("Spettacolo",new ArrayList<String>());
					categories.put("Sport",new ArrayList<String>());
					categories.put("Tecnologia",new ArrayList<String>());
					categories.put("Scienza",new ArrayList<String>());
					categories.put("Politica",new ArrayList<String>());
					categories.put("Business",new ArrayList<String>());
					categories.put("Salute",new ArrayList<String>());
					categories.put("Arte",new ArrayList<String>());
				
					categories_table.commit();
				}
			}
			return categories_table;
		}
	}
	/*Metodo che restituisce un String[] contenente le categorie (string) presenti sul sito, 
	 * mostrate in fase di registrazione all'utente per fargli esprimere le preferenze*/
	public String[] getCategoriesArray() {
		DB db = getCategories();
		Map<String,ArrayList<String>> categories = db.getTreeMap("category");

		String[] categoriesArray = new String[categories.size()]; 
		int i = 0;
		for (Map.Entry<String,ArrayList<String>> entry : categories.entrySet()) {
			categoriesArray[i] = entry.getKey();
			i++;
		}

		return categoriesArray;
	}
	/*Metodo che restituisce un ArrayList<Category> utilizzato per la gestione delle categorie e le rispettive sottocategorie*/
	public ArrayList<Category> getCategoriesArrayList(){
		ArrayList<Category> categoriesList = new ArrayList<Category>();

		DB db = getCategories();
		Map<String,ArrayList<String>> categories = db.getTreeMap("category");
		for (Map.Entry<String,ArrayList<String>> entry : categories.entrySet()) {
			categoriesList.add(new Category(entry.getKey(),entry.getValue()));
		}
		
		return categoriesList;
	}
	
	/*Metodo che restituisce tutti gli articoli sia FREE che PREMIUM.
	 * Come paramentro di ingresso necessita di un ArrayList<String> : 
	 * 		- Se pieno verrano visualizzati gli articoli delle categorie preferite dall'utente;
	 * 		- Altrimenti, tutti gli articoli presenti sul sito */
	public ArrayList<Article> getArticlesForUser(ArrayList<String> favouriteCategories){
		ArrayList<Article> articles = new ArrayList<Article>();
		
		DB db = getArticles();
		Map<Integer, Article> map = db.getTreeMap("article");

		for (Map.Entry<Integer, Article> entry : map.entrySet()) {
			if(entry.getValue().getTitle() != null) {
				if(favouriteCategories.size() != 0) {
					if(favouriteCategories.indexOf(entry.getValue().getCategory()) != -1) {
						articles.add(entry.getValue());
					}
				} else {
					articles.add(entry.getValue());
				}
			}
		}
		/*Comparator che consente di ordinare l'ArrayList<Article> in maniera non crescente gli articoli in base alla data di creazione*/
		Comparator<Article> comparatorArticles = Comparator.comparing(Article :: getDateCreation);
		Collections.sort(articles, comparatorArticles);
		return articles;
	}
	/*Metodo che restituisce un ArrayList<String> contenente gli ID degli articoli che un determinato utente ha commentato o messo mi piace*/
	public ArrayList<String> getArticlesLikedAndCommentedByUser(String nicknameUser){
		DB db = getTable();
		Map<Integer, User> map = db.getTreeMap("user");
		
		for (Map.Entry<Integer, User> entry : map.entrySet()) {
			if(entry.getValue().getNickname().equals(nicknameUser)) {
				return entry.getValue().getLikesAndComments();
			}
		}
		return null;
	}
	/*Metodo che restituisce un Article tramite il suo ID*/
	public Article getArticleById(int idArticles){ 
		DB db = getArticles();
		Map<Integer, Article> map = db.getTreeMap("article");

		for (Map.Entry<Integer, Article> entry : map.entrySet()) {
			if(entry.getValue().getId() != null) {
				if(entry.getValue().getId() == idArticles) {
					return entry.getValue();
				}
			}
		}
		return null;
	}
	/*Metodo che consente la pubblicazione di un commento.*/
	public Boolean postComment(Article article, String userNickname, String commentText) {
		DB db = getArticles();
		Map<Integer, Article> map = db.getTreeMap("article");

		for (Map.Entry<Integer, Article> entry : map.entrySet()) {
			if(entry.getValue().getId() == article.getId()) {
				/*Il commento viene aggiunto all'ArrayList<Comment> dell'articolo*/
				entry.getValue().getComments().add(new Comment(entry.getValue().getComments().size(), userNickname, commentText)); //...
				
				map.put(Collections.max(map.keySet()) + 1, new Article());
				db.commit();
				map.remove(Collections.max(map.keySet()));
				db.commit();
				
				
				DB dbU = getTable();
				Map<Integer, User> users = dbU.getTreeMap("user");
				for (Map.Entry<Integer, User> user : users.entrySet()) {
					if(user.getValue().getNickname().equals(userNickname)) {
						/*L'azione del commento viene registrata nell'ArrayList<String> likesAndComments dell'utente*/
						user.getValue().getLikesAndComments().add(article.getId() + "&*&" + "Hai commentato l'articolo: " + article.getTitle());
					}
				}
				users.put(Collections.max(users.keySet()) + 1, new User());
				dbU.commit();
				users.remove(Collections.max(users.keySet()));
				dbU.commit();
				return true;
			}
		}
		return false;
	}
	/*Metodo che consente di mettere mi piace ad un articolo*/
	public Boolean likeArticle(String nicknameUser, Article article) {
		DB db = getArticles();
		Map<Integer, Article> map = db.getTreeMap("article");
		
		boolean disLike = false;
		Integer keyMapArticle = null;
		
		for (Map.Entry<Integer, Article> entry : map.entrySet()) {
			if(entry.getValue().getId() == article.getId()) {
				keyMapArticle = entry.getKey(); 
				for(int i = 0; i < entry.getValue().getLikes().size(); i++) {
					if(entry.getValue().getLikes().get(i).equals(nicknameUser)) {
						/*Fase di rimozione del LIKE*/
						disLike = true;
						article.getLikes().remove(nicknameUser);
						map.put(keyMapArticle, article);
						
						DB dbU = getTable();
						Map<Integer, User> users = dbU.getTreeMap("user");
						for (Map.Entry<Integer, User> user : users.entrySet()) {
							if(user.getValue().getNickname().equals(nicknameUser)) {
								/*L'azione del mi piace viene registrata nell'ArrayList<String> likesAndComments dell'utente*/
								user.getValue().getLikesAndComments().add(article.getId() + "&*&" + "Hai tolto il like all'articolo: " + article.getTitle());
							}
						}
						users.put(Collections.max(users.keySet()) + 1, new User());
						dbU.commit();
						users.remove(Collections.max(users.keySet()));
						dbU.commit();
					}
				}
			}
		}
		if(!disLike) {
			/*Fase di aggiunta del LIKE*/
			article.getLikes().add(nicknameUser);
			map.put(keyMapArticle, article);
			
			DB dbU = getTable();
			Map<Integer, User> users = dbU.getTreeMap("user");
			for (Map.Entry<Integer, User> user : users.entrySet()) {
				if(user.getValue().getNickname().equals(nicknameUser)) {
					/*L'azione del mi piace viene registrata nell'ArrayList<String> likesAndComments dell'utente*/
					user.getValue().getLikesAndComments().add(article.getId() + "&*&" + "Hai messo like all'articolo: " + article.getTitle());
				}
			}
			users.put(Collections.max(users.keySet()) + 1, new User());
			dbU.commit();
			users.remove(Collections.max(users.keySet()));
			dbU.commit();
		}
		db.commit();
		return !disLike;
	}
	/*Metodo che restituisce la MAPPA (tabella) degli Articoli*/
	private DB getArticles() {
		ServletContext context = this.getServletContext();
		synchronized (context) {
			DB articles_table = (DB)context.getAttribute("ARTICLES_TABLE");
			if(articles_table == null) {
				articles_table = DBMaker.newFileDB(new File("article_table")).closeOnJvmShutdown().make();
				context.setAttribute("ARTICLES_TABLE", articles_table);
			}
			return articles_table;
		}
	}
}
