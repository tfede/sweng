package bolognino.server;

import javax.servlet.http.HttpSession;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import bolognino.client.MySessionService;
import bolognino.client.User;


public class MySessionServiceImpl extends RemoteServiceServlet implements MySessionService {
    HttpSession httpSession;
    
    /*Metodo che consente di "settare" la sessione per uno specifico utente*/
    @Override
    public void setSession(User user) {
        httpSession = getThreadLocalRequest().getSession();
        httpSession = this.getThreadLocalRequest().getSession();
        httpSession.setAttribute("user", user);

    }
    /*Metodo che restituisce l'Utente presente nella sessione*/
    @Override
    public User getSession() {
    	httpSession = getThreadLocalRequest().getSession();
        httpSession = this.getThreadLocalRequest().getSession();
        if(this.getThreadLocalRequest().getSession().getAttribute("user") != null) {
        	return (User) this.getThreadLocalRequest().getSession().getAttribute("user");
        }
        return null;
    }
    /*Metodo che distrugge la sessione --> LOGOUT*/
    @Override
    public void exitSession() {
          httpSession = this.getThreadLocalRequest().getSession();
          httpSession.invalidate(); // kill session     
    }
}


