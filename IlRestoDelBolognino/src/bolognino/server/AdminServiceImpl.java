package bolognino.server;

import java.io.File;
import java.util.Map;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.ServletContext;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import bolognino.client.Admin;
import bolognino.client.AdminService;
import bolognino.client.Article;
import bolognino.client.Author;
import bolognino.client.Category;
import bolognino.client.User;

public class AdminServiceImpl extends RemoteServiceServlet implements AdminService {
	
	/*public ManageCategoryPage2 getManageCategoryPage(){
		return new ManageCategoryPage2();
	}*/
	/*Metodo che consente l'approvazione di un commento pubblicato da un utente registrato sotto ad un articolo*/
	public Boolean approveComment(int idArticle, int idComment) {
		DB db = getArticles();
		Map<Integer, Article> map = db.getTreeMap("article");
		for (Map.Entry<Integer, Article> entry : map.entrySet()) {
			if(entry.getValue().getTitle() != null) {
				if(entry.getValue().getId() == idArticle) {
					for(int i = 0; i < entry.getValue().getComments().size(); i++) {
						if(entry.getValue().getComments().get(i).getId() == idComment) {
							/*Commento che viene aggiunto ai commenti moderati dell'articolo*/
							entry.getValue().getModeratedComments().add(entry.getValue().getComments().get(i));
							/*Commento che viene rimosso dai commenti da moderare dell'articolo*/
							entry.getValue().getComments().remove(i);
							
							map.put(Collections.max(map.keySet()) + 1, new Article());
							db.commit();
							map.remove(Collections.max(map.keySet()));
							
							db.commit();
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	/*Metodo che consente l'disapprovazione di un commento pubblicato da un utente registrato sotto ad un articolo*/
	public Boolean declineComment(int idArticle, int idComment) {
		DB db = getArticles();
		Map<Integer, Article> map = db.getTreeMap("article");
		for (Map.Entry<Integer, Article> entry : map.entrySet()) {
			if(entry.getValue().getTitle() != null) {
				if(entry.getValue().getId() == idArticle) {
					for(int i = 0; i < entry.getValue().getComments().size(); i++) {
						if(entry.getValue().getComments().get(i).getId() == idComment) {
							/*Commento che viene aggiunto ai commenti respinti dell'articolo*/
							entry.getValue().getDeclinedComments().add(entry.getValue().getComments().get(i));
							/*Commento che viene rimosso dai commenti da moderare dell'articolo*/
							entry.getValue().getComments().remove(i);
							
							map.put(Collections.max(map.keySet()) + 1, new Article());
							db.commit();
							map.remove(Collections.max(map.keySet()));
							db.commit();
							
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	/*Metodo che consente la rimozione di un commento di un articolo, moderato precedentemente*/
	public Boolean removeComment(int idArticle, int idComment) {
		DB db = getArticles();
		Map<Integer, Article> map = db.getTreeMap("article");
		for (Map.Entry<Integer, Article> entry : map.entrySet()) {
			if(entry.getValue().getTitle() != null) {
				if(entry.getValue().getId() == idArticle) {
					for(int i = 0; i < entry.getValue().getModeratedComments().size(); i++) {
						if(entry.getValue().getModeratedComments().get(i).getId() == idComment) {
							/*Commento rimosso dai commenti moderati*/
							entry.getValue().getModeratedComments().remove(i);
							
							map.put(Collections.max(map.keySet()) + 1, new Article());
							db.commit();
							map.remove(Collections.max(map.keySet()));
							db.commit();
							
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	/*Metodo che restituisce un ArrayList<Article> i quali possiedono almeno un commento da moderare*/
	public ArrayList<Article> getArticlesWithCommentsToModerate(){
		ArrayList<Article> articles = new ArrayList<Article>();
		DB db = getArticles();
		Map<Integer, Article> map = db.getTreeMap("article");
		
		for (Map.Entry<Integer, Article> entry : map.entrySet()) {
			if(entry.getValue().getTitle() != null) {
				if(entry.getValue().getComments().size() != 0) {
					articles.add(entry.getValue());
				}
			}
		}
		return articles;
	}
	
	
	public Boolean replaceArticles(Map<Integer, Article> articles) {
		DB db = getArticles();
		Map<Integer, Article> map = db.getTreeMap("article");
		
		ArrayList<Article> oldArticles = new ArrayList<Article>();
		for (Map.Entry<Integer,Article> entry : map.entrySet()) {
			if(entry.getValue().getTitle() != null) {
				oldArticles.add(entry.getValue());
			}
		}
		
		//map.clear();
		for (Map.Entry<Integer,Article> entry : articles.entrySet()) {
			if(entry.getValue().getTitle() != null) {
				for (Map.Entry<Integer,Article> entry2 : map.entrySet()) {
					if(entry2.getValue().getTitle() != null) {
						if(entry2.getValue().getId() == entry.getValue().getId()) {
							map.put(entry.getKey(), entry.getValue());
							oldArticles.remove(oldArticles.indexOf(entry2.getValue())); //?
						}
					}
				}
			}
		}
		
		for (Map.Entry<Integer,Article> entry : map.entrySet()) {
			for(int i = 0; i < oldArticles.size(); i++) {
				if(entry.getValue().getTitle() != null) {
					if(oldArticles.get(i).equals(entry.getValue())) {
						map.remove(entry.getKey());
					}
				}
			}
		}

		db.commit();
		return true;
	}
	/*Metodo che restituisce tutti gli articoli presenti sul sito*/
	public ArrayList<Article> getAllArticles(){
		ArrayList<Article> articles = new ArrayList<Article>();
		DB db = getArticles();
		Map<Integer, Article> map = db.getTreeMap("article");

		for (Map.Entry<Integer, Article> entry : map.entrySet()) {
			if(entry.getValue().getTitle() != null) {
				articles.add(entry.getValue());
			}
		}
		return articles;
	}
	/*Metodo che restituisce tutti gli autori registrati sul sito*/
	public ArrayList<User> getAuthorRegistred(){
		ArrayList<User> authors = new ArrayList<User>();
		DB db = getTable();		//utenti
		Map<Integer, User> map = db.getTreeMap("user");
		
		for (Map.Entry<Integer, User> entry : map.entrySet()) {
			if(entry.getValue() instanceof Author) {
				authors.add(entry.getValue());
			}
		}
		
		return authors;
	}
	/*Metodo che consente l'aggiornamento successivo a modifiche delle categorie e delle relative sottocategorie*/
	public Boolean replaceCategories(Map<String,ArrayList<String>> newCategories) {
		DB db = getCategories();
		Map<String,ArrayList<String>> categories = db.getTreeMap("category");

		//Clear categories
		categories.clear();

		for (Map.Entry<String,ArrayList<String>> entry : newCategories.entrySet()) {
			categories.put(entry.getKey(),entry.getValue());
		}

		db.commit();

		return true;
	}
	/*Metodo che restituisce la MAPPA (tabella) degli Articoli */
	private DB getArticles() {
		ServletContext context = this.getServletContext();
		synchronized (context) {
			DB articles_table = (DB)context.getAttribute("ARTICLES_TABLE");
			if(articles_table == null) {
				//ENTRA ANCHE SE LA TABLE ESISTE QUINDI NON SERVE A NIENTE QUESTO CONTROLLO.
				articles_table = DBMaker.newFileDB(new File("article_table")).closeOnJvmShutdown().make();
				context.setAttribute("ARTICLES_TABLE", articles_table);
			}
			return articles_table;
		}
	}
	/*Metodo che restituisce la MAPPA (tabella) delle Categorie*/
	private DB getCategories() {
		ServletContext context = this.getServletContext();
		synchronized (context) {
			DB categories_table = (DB)context.getAttribute("CATEGORIES_TABLE");
			
			if(categories_table == null) {	
				categories_table = DBMaker.newFileDB(new File("categories_table")).closeOnJvmShutdown().make();
				context.setAttribute("CATEGORIES_TABLE", categories_table);
				Map<String,ArrayList<String>> categories = categories_table.getTreeMap("category");
				/*Se la tabella delle categorie � vuota viene riempita con le 10 categorie di default*/
				if(categories.size() == 0) {
					categories.put("Italia",new ArrayList<String>());
					categories.put("Mondo",new ArrayList<String>());
					categories.put("Spettacolo",new ArrayList<String>());
					categories.put("Sport",new ArrayList<String>());
					categories.put("Tecnologia",new ArrayList<String>());
					categories.put("Scienza",new ArrayList<String>());
					categories.put("Politica",new ArrayList<String>());
					categories.put("Business",new ArrayList<String>());
					categories.put("Salute",new ArrayList<String>());
					categories.put("Arte",new ArrayList<String>());
				
					categories_table.commit();
				}
			}
			return categories_table;
		}
	}
	/*Metodo che restituisce un ArrayList<Category> utilizzato per la gestione delle categorie e le rispettive sottocategorie*/
	public ArrayList<Category> getCategoriesList(){
		ArrayList<Category> categoriesList = new ArrayList<Category>();
		DB db = getCategories();
		Map<String,ArrayList<String>> categories = db.getTreeMap("category");
		for (Map.Entry<String,ArrayList<String>> entry : categories.entrySet()) {
			categoriesList.add(new Category(entry.getKey(),entry.getValue()));
		}
		return categoriesList;
	}
	/*Controllo di unicit� della Categoria*/
	public boolean uniqueCategory(Map<String,ArrayList<String>> map, String category){
		if(map.entrySet().stream().filter(x -> category.equals(x.getKey())).findFirst().isPresent()) {
			return false;
		}
		return true; 		// inserimento da effettuare
	}
	/*Metodo che restituisce la MAPPA (tabella) degli Utenti*/
	private DB getTable() {
		ServletContext context = this.getServletContext();
		synchronized (context) {
			DB user_table = (DB)context.getAttribute("USER_TABLE");
			if(user_table == null) {
				user_table = DBMaker.newFileDB(new File("user_table")).closeOnJvmShutdown().make();
				context.setAttribute("USER_TABLE", user_table);
				
				Map<Integer,User> users = user_table.getTreeMap("user");
				/*Se la tabella degli utenti � vuota viene riempita con un unico utente, ovvero l'Admin con NICKNAME admin e PASSWORD admin*/
				if(users.size() == 0) {
					users.put(users.size(),new Admin("admin","admin"));
					user_table.commit();
				}
				
			}
			return user_table;
		}
	}
}
