package bolognino.server;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletContext;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import bolognino.client.Article;
import bolognino.client.ArticlePremium;
import bolognino.client.Author;
import bolognino.client.AuthorService;
import bolognino.client.Comment;
import bolognino.client.User;

public class AuthorServiceImpl extends RemoteServiceServlet implements AuthorService {
	/*Metodo che consente di pubblicare un nuovo articolo FREE con la memorizzazione nella mappa degli Articoli (Map<Integer,Article>)*/
	public Boolean newArticle(Author author, String title, String content, String category, String subCategory) {
		DB db = getArticles();
		Map<Integer, Article> map = db.getTreeMap("article");
		Date date = new Date();
		if(map.size() == 0) {
			Article article = new Article(map.size(), author, title, content, category, subCategory, date.getTime() / 1000L, new ArrayList<String>() , new ArrayList<Comment>(), new ArrayList<Comment>(), new ArrayList<Comment>());
			map.put(map.size(), article); 
		}else {
			Article article = new Article(Collections.max(map.keySet()) + 1, author, title, content, category, subCategory, date.getTime() / 1000L, new ArrayList<String>(), new ArrayList<Comment>(), new ArrayList<Comment>(), new ArrayList<Comment>());
			map.put(Collections.max(map.keySet()) + 1, article); 
		}
		
		db.commit();
		return true;
	}
	/*Metodo che consente di pubblicare un nuovo articolo PREMIUM con la memorizzazione nella mappa degli Articoli (Map<Integer,Article>)*/
	public Boolean newArticlePremium(Author author, String title, String content, String category, String subCategory) {
		DB db = getArticles();
		Map<Integer, Article> map = db.getTreeMap("article");
		Date date = new Date();

		if(map.size() == 0) {
			Article article = new ArticlePremium(map.size(),author, title, content, category, subCategory, date.getTime() / 1000L, new ArrayList<String>(), new ArrayList<Comment>(), new ArrayList<Comment>(), new ArrayList<Comment>());
			map.put(map.size(), article); 
		}else {
			Article article = new ArticlePremium(Collections.max(map.keySet()) + 1, author, title, content, category, subCategory, date.getTime() / 1000L, new ArrayList<String>(), new ArrayList<Comment>(), new ArrayList<Comment>(), new ArrayList<Comment>());
			map.put(Collections.max(map.keySet()) + 1, article); 
		}
		db.commit();
		return true;
	}
	/*Metodo che restituisce tutti gli articoli di un autore passato come parametro di ingresso*/
	public ArrayList<Article> getArticlesByAuthor(String nicknameAuthor){
		ArrayList<Article> articles = new ArrayList<Article>();
		
		DB db = getArticles();
		Map<Integer, Article> map = db.getTreeMap("article");
		for (Map.Entry<Integer, Article> entry : map.entrySet()) {
			if(entry.getValue().getTitle() != null) {
				if(entry.getValue().getAuthor().getNickname().equals(nicknameAuthor)) {
					articles.add(entry.getValue());
				}
			}
		}
		return articles;
	}
	/*Metodo che restituisce un Article tramite il suo ID*/
	public Article getArticleById(int idArticles){ 
		DB db = getArticles();
		Map<Integer, Article> map = db.getTreeMap("article");

		for (Map.Entry<Integer, Article> entry : map.entrySet()) {
			if(entry.getValue().getId() != null) {
				if(entry.getValue().getId() == idArticles) {
					return entry.getValue();
				}
			}
		}
		return null;
	}
	/*Metodo che consente di modificare un articolo con, successivamente, il rimpiazzamento nella mappa degli Articoli (Map<Integer,Article>)*/
	public Boolean editArticle(Article article) {
		DB db = getArticles();
		Map<Integer, Article> map = db.getTreeMap("article");
		for (Map.Entry<Integer, Article> entry : map.entrySet()) {
			if(entry.getValue().getTitle() != null) {
				if(entry.getValue().getId() == article.getId()) {
					map.put(entry.getKey(), article);

					map.put(Collections.max(map.keySet()) + 1, new Article());
					db.commit();
					map.remove(Collections.max(map.keySet()));
					db.commit();

					return true;
				}
			}
		}
		return false;
	}
	/*Metodo che consente di eliminare un articolo dalla mappa degli Articoli (Map<Integer,Article>)*/
	public Boolean deleteArticle(int idArticle) {
		DB db = getArticles();
		Map<Integer, Article> map = db.getTreeMap("article");
		for (Map.Entry<Integer, Article> entry : map.entrySet()) {
			if(entry.getValue().getTitle() != null) {
				if(entry.getValue().getId() == idArticle) {
					map.put(entry.getKey(),new Article());
					//map.remove(entry.getKey());
					db.commit();
					return true;
				}
			}
		}
		return false;
	}
	/*Metodo che restituisce la MAPPA (tabella) degli Utenti */
	private DB getArticles() {
		ServletContext context = this.getServletContext();
		synchronized (context) {
			DB articles_table = (DB)context.getAttribute("ARTICLES_TABLE");
			if(articles_table == null) {
				//ENTRA ANCHE SE LA TABLE ESISTE QUINDI NON SERVE A NIENTE QUESTO CONTROLLO.
				articles_table = DBMaker.newFileDB(new File("article_table")).closeOnJvmShutdown().make();
				context.setAttribute("ARTICLES_TABLE", articles_table);
			}
			return articles_table;
		}
	}
}
