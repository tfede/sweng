package bolognino.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;


public interface AuthorServiceAsync {

	void newArticle(Author author, String title, String content, String category, String subCategory, AsyncCallback<Boolean> callback);
	
	void newArticlePremium(Author author, String title, String content, String category, String subCategory, AsyncCallback<Boolean> callback);
	
	void getArticlesByAuthor(String nicknameAuthor, AsyncCallback<ArrayList<Article>> callback);
	
	void getArticleById(int idArticles, AsyncCallback<Article> callback);
	
	void editArticle(Article article, AsyncCallback<Boolean> callback);
	
	void deleteArticle(int idArticle, AsyncCallback<Boolean> callback);
}
