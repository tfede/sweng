package bolognino.client;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class HomePage implements Serializable{
	
	public HomePage() {
		
	}
	
	public VerticalPanel getHomePage() {
	    UserServiceAsync userSvc = GWT.create(UserService.class);
		VerticalPanel mainPanel = new VerticalPanel();
		HorizontalPanel buttonsHorizontalPanel = new HorizontalPanel();
		buttonsHorizontalPanel.addStyleName("pure-menu-item");
		Button registrationUserButton = new Button("Registrati come utente");
		registrationUserButton.addStyleName("btn");
		Button registrationAuthorButton = new Button("Registrati come scrittore");
		registrationAuthorButton.addStyleName("btn");
		Button loginButton = new Button("Login");
		loginButton.addStyleName("btn");
		
		registrationUserButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	userSvc.getRegistrationUserPage( new AsyncCallback<RegistrationUserPage>() {
					@Override
					public void onFailure(Throwable caught) {
						Window.alert(caught.getMessage());
					}
					@Override
					public void onSuccess(RegistrationUserPage result) {
						RootPanel.get("mainContent").clear();
						RootPanel.get("mainContent").add(result.getRegistrationUserPage());
					}
				});
            }
		});
		
		registrationAuthorButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	userSvc.getAccessRegistrationAuthorPage( new AsyncCallback<AccessRegistrationAuthorPage>() {
					@Override
					public void onFailure(Throwable caught) {
						Window.alert(caught.getMessage());
					}
					@Override
					public void onSuccess(AccessRegistrationAuthorPage result) {
						//RootPanel.get("mainContent").add(mainPanel);
						RootPanel.get("mainContent").clear();
						RootPanel.get("mainContent").add(result.getAccessRegistrationAuthorPage());
					}
				});
            }
		});
		
		loginButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	userSvc.getLoginPage( new AsyncCallback<LoginPage>() {
					@Override
					public void onFailure(Throwable caught) {
						Window.alert(caught.getMessage());
					}
					@Override
					public void onSuccess(LoginPage result) {
						//RootPanel.get("mainContent").add(mainPanel);
						RootPanel.get("mainContent").clear();
						RootPanel.get("mainContent").add(result.getLoginPage());
					}
				});
            }
		});
		
		buttonsHorizontalPanel.add(registrationUserButton);
		buttonsHorizontalPanel.add(registrationAuthorButton);
		buttonsHorizontalPanel.add(loginButton);
		
		mainPanel.add(buttonsHorizontalPanel);
		
		mainPanel.add(getArticles());
		
		return mainPanel;
	}
	
	public VerticalPanel getArticles() {
		VerticalPanel articlesPanel = new VerticalPanel();
		UserServiceAsync userSvc = GWT.create(UserService.class);
		
		userSvc.getArticlesForUser(new ArrayList<String>(), new AsyncCallback<ArrayList<Article>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Article> result) {
				for(int i = result.size() - 1; i >= 0; i--) {
					articlesPanel.add(result.get(i).getArticlePreview(result.get(i)));
				}
			}
		});
		return articlesPanel;
	}

}
