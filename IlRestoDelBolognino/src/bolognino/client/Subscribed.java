package bolognino.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Subscribed extends User implements Serializable { //Ogni qualvolta fa l'accesso si controlla che il suo abbonamento non sia scaduto, se dovesse essere scaduto lo si declassa a user, cambiando la sua cella (key) dell'HashMap prima di chiamare il metodo per il reindirizzamento
	private String ncc;
	private String cardHolderName;
	private String cardHolderSurname;
	private Date cardDeadline;
	private String cvv;
	private String nation;
	private String cap;
	
	public Subscribed() {
		
	}

	public Subscribed(String nickname, String password, String email, String firstname, String surname, Date birthday, String state, String province, String region, String city, String address, Boolean gender, String birthPlace, ArrayList<String> categories, ArrayList<String> likesAndComments, String ncc, String cardHolderName, String cardHolderSurname, Date cardDeadline, String cvv, String nation, String cap) {
		super(nickname, password, email, firstname, surname, birthday, state, province, region, city, address, gender, birthPlace, categories, likesAndComments);
		this.ncc = ncc;
		this.cardHolderName = cardHolderName;
		this.cardHolderSurname = cardHolderSurname;
		this.cardDeadline = cardDeadline;
		this.cvv = cvv;
		this.nation = nation;
		this.cap = cap;
	}

	public String getNCC() {
		return ncc;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public String getCardHolderSurname() {
		return cardHolderSurname;
	}

	public Date getCardDeadline() {
		return cardDeadline;
	}

	public String getCVV() {
		return cvv;
	}

	public String getNation() {
		return nation;
	}

	public String getCAP() {
		return cap;
	}
	
	//articoli premium con possobilit� di commento... + getProfilePage()
	@Override
	public VerticalPanel getHomePage() { //ritorna la pagina principale per un utente iscritto abbonato
		VerticalPanel mainPanel = new VerticalPanel();
		VerticalPanel articlePanel = new VerticalPanel();

		HorizontalPanel buttonsPanel = new HorizontalPanel();
		Button showYourInfo = new Button("Visualizza il tuo profilo");
		showYourInfo.addStyleName("btn");
		Button logout = new Button("Logout");
		logout.addStyleName("btn");
		
		HorizontalPanel articleButtonsPanel = new HorizontalPanel();
		RadioButton favouriteArticlesRButton = new RadioButton("myRadioGroup", "Gli articoli per te");
		favouriteArticlesRButton.setChecked(true);
		RadioButton allArticlesRButton = new RadioButton("myRadioGroup", "Ultime News");
		
		showYourInfo.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	RootPanel.get("mainContent").clear();
	        	
	        	RootPanel.get("mainContent").add(getProfilePage());
	        }
	    });
		
		logout.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	UserServiceAsync userSvc = GWT.create(UserService.class);
	        	userSvc.getHomePage( new AsyncCallback<HomePage>() {
        			@Override
        			public void onFailure(Throwable caught) {
        				Window.alert(caught.getMessage());
        			}
        			@Override
        			public void onSuccess(HomePage result) {
        				MySessionServiceAsync sessionSvc = GWT.create(MySessionService.class);
        		        sessionSvc.exitSession( new AsyncCallback<Void>() {
        		            @Override
        		            public void onSuccess(Void result2) {
        		            	RootPanel.get("mainContent").clear();
                				RootPanel.get("mainContent").add(result.getHomePage());
        		            }
        		            @Override
        		            public void onFailure(Throwable caught) {
        		                Window.alert(caught.getMessage());
        		            }
        		        });
        			}
        		});
	        }
	    });
		
		favouriteArticlesRButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	articlePanel.clear();
	        	articlePanel.add(getArticles(getCategories()));
	        	RootPanel.get("mainContent").add(articlePanel);
	        }
	    });
		
		allArticlesRButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	articlePanel.clear();
	        	articlePanel.add(getArticles(new ArrayList<String>()));
	        	RootPanel.get("mainContent").add(articlePanel);
	        }
	    });
		
		Label welcome = new Label("Bentornato: " + getNickname());
		welcome.addStyleName("welcome");
		mainPanel.add(welcome);
		
		buttonsPanel.add(showYourInfo);
		buttonsPanel.add(logout);
		mainPanel.add(buttonsPanel);
		
		articleButtonsPanel.add(favouriteArticlesRButton);
		articleButtonsPanel.add(allArticlesRButton);
		
		mainPanel.add(articleButtonsPanel);
		
		articlePanel.add(getArticles(getCategories()));
		
		mainPanel.add(articlePanel);
		
		return mainPanel;
	}
	
	@Override
	public VerticalPanel getProfilePage() { //ritorna la pagina principale per un utente che ha sottoscritto un abbonamento
		VerticalPanel mainPanel = new VerticalPanel();

		HorizontalPanel buttonsPanel = new HorizontalPanel();
		Button buttonHomePage = new Button("Torna indietro");
		buttonHomePage.addStyleName("btn");
		Button showYourInfoButton = new Button("Visualizza dati inseriti");
		showYourInfoButton.addStyleName("btn");
		Button showYourInfoSubButton = new Button("Visualizza dati abbonamento");
		showYourInfoSubButton.addStyleName("btn");
		Button unsubscribedButton = new Button("Disdici l'abbonamento al giornale");
		unsubscribedButton.addStyleName("btn");

		buttonHomePage.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	RootPanel.get("mainContent").clear();
            	RootPanel.get("mainContent").add(getHomePage());
            }
		});
		
		showYourInfoButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	RootPanel.get("mainContent").clear();

	        	RootPanel.get("mainContent").add(getUserInformationPage());
	        }
	    });
		
		showYourInfoSubButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	RootPanel.get("mainContent").clear();
	        	
	        	RootPanel.get("mainContent").add(getUserInformationSubPage());
	        }
	    });
		
		unsubscribedButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	if(Window.confirm("Sei sicuro di disdire l'abbonamento?")) {
	        		RootPanel.get("mainContent").clear();
	        		UserServiceAsync userSvc = GWT.create(UserService.class);
	        		userSvc.getUserByNickname(getNickname(), new AsyncCallback<User>() {
	        			@Override
	        			public void onFailure(Throwable caught) {
	        				Window.alert("Cannot load symbols: " + caught.getMessage());
	        			}
	        			@Override
	        			public void onSuccess(User user) {
	        				userSvc.downgradeUser(user, new AsyncCallback<User>() {
	    	        			@Override
	    	        			public void onFailure(Throwable caught) {
	    	        				Window.alert("Cannot load symbols: " + caught.getMessage());
	    	        			}
	    	        			@Override
	    	        			public void onSuccess(User user) {
	    	        				if(user != null) {
	    	        					MySessionServiceAsync sessionSvc = GWT.create(MySessionService.class);
	    		        		        sessionSvc.exitSession( new AsyncCallback<Void>() {
	    		        		            @Override
	    		        		            public void onSuccess(Void result) {
	    		        		            	MySessionServiceAsync sessionSvc = GWT.create(MySessionService.class);
	    		    	        		        sessionSvc.setSession( user, new AsyncCallback<Void>() {
	    		    	        		            @Override
	    		    	        		            public void onSuccess(Void result2) {
	    		    	        		            	
	    		    	        		            }
	    		    	        		            @Override
	    		    	        		            public void onFailure(Throwable caught) {
	    		    	        		                Window.alert(caught.getMessage());
	    		    	        		            }
	    		    	        		        });
	    		        		            }
	    		        		            @Override
	    		        		            public void onFailure(Throwable caught) {
	    		        		                Window.alert(caught.getMessage());
	    		        		            }
	    		        		        });
	    	        					RootPanel.get("mainContent").add(user.getProfilePage());
	    	        				}else {
	    	        					Window.alert("Il downgrade del profilo non � andata a buon fine. Riprovare");
	    	        					RootPanel.get("mainContent").add(getProfilePage());
	    	        				}
	    	        			}
	    	        		});
	        			}
	        		});
	        	}
	        }
	    });
		
		Label welcome = new Label("Bentornato: " + getNickname());
		welcome.addStyleName("welcome");
		mainPanel.add(welcome);
		buttonsPanel.add(buttonHomePage);
		buttonsPanel.add(showYourInfoButton);
		buttonsPanel.add(showYourInfoSubButton);
		buttonsPanel.add(unsubscribedButton);
		mainPanel.add(buttonsPanel);
		
		mainPanel.add(getYourLikesAndComments(getNickname()));
		
		return mainPanel;
	}
	
	public VerticalPanel getUserInformationSubPage() {
		VerticalPanel yourInfoSubPanel = new VerticalPanel();
		
		Button buttonHomePage = new Button("Torna al profilo");
		buttonHomePage.addStyleName("btn");
		
		buttonHomePage.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	RootPanel.get("mainContent").clear();
            	RootPanel.get("mainContent").add(getProfilePage());
            }
		});
		yourInfoSubPanel.add(buttonHomePage);
		
		Label nccLabel = new Label("Numero della carta:");
		nccLabel.addStyleName("label");
		yourInfoSubPanel.add(nccLabel);
		yourInfoSubPanel.add(new Label(getNCC()));
		Label nameHolderLabel = new Label("Nome del titolare");
		nameHolderLabel.addStyleName("label");
		yourInfoSubPanel.add(nameHolderLabel);
		yourInfoSubPanel.add(new Label(getCardHolderName()));
		Label surnameHolderLabel = new Label("Cognome del titolare:");
		surnameHolderLabel.addStyleName("label");
		yourInfoSubPanel.add(surnameHolderLabel );
		yourInfoSubPanel.add(new Label(getCardHolderSurname()));
		Label deadlineCardLabel = new Label("Data di scadenza:");
		deadlineCardLabel.addStyleName("label");
		yourInfoSubPanel.add(deadlineCardLabel);
    	
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		yourInfoSubPanel.add(new Label(dateFormat.format(getCardDeadline())));
		
		Label cvvLabel = new Label("CVV:");
		cvvLabel.addStyleName("label");
		
		yourInfoSubPanel.add(cvvLabel);
		yourInfoSubPanel.add(new Label(getCVV()));
		Label nationLabel = new Label("Nazione:");
		nationLabel.addStyleName("label");
		yourInfoSubPanel.add(nationLabel);
		yourInfoSubPanel.add(new Label(getNation()));
		Label capLabel = new Label("CAP:");
		capLabel.addStyleName("label");
		yourInfoSubPanel.add(capLabel);
		yourInfoSubPanel.add(new Label(getCAP()));
		
		return yourInfoSubPanel;
	}
	
	public VerticalPanel getArticles(ArrayList<String> favouriteCategories) {
		VerticalPanel articlesPanel = new VerticalPanel();
		UserServiceAsync userSvc = GWT.create(UserService.class);
		
		userSvc.getArticlesForUser(favouriteCategories, new AsyncCallback<ArrayList<Article>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Cannot load symbols: " + caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Article> result) {
				for(int i = result.size() - 1; i >= 0; i--) {
					articlesPanel.add(result.get(i).getArticlePreview(result.get(i)));
				}
			}
		});
		return articlesPanel;
	}
}