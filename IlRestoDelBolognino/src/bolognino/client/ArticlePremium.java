package bolognino.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class ArticlePremium extends Article implements Serializable {
	
	public ArticlePremium() {
		
	}
	
	public ArticlePremium(Integer id, Author author, String title, String content, String category, String subCategory, long now, ArrayList<String> likes, ArrayList<Comment> comments, ArrayList<Comment> moderatedComments, ArrayList<Comment> declinedComments) {
		super(id, author, title, content, category, subCategory, now, likes, comments, moderatedComments, declinedComments);		
	}
	
	@Override
	public VerticalPanel getArticlePreview(Article article) {
		MySessionServiceAsync sessionSvc = GWT.create(MySessionService.class);
		UserServiceAsync userSvc = GWT.create(UserService.class);
		VerticalPanel articlePanel = new VerticalPanel();
		
		Hyperlink linkTitle = new Hyperlink(getTitle() + " *", "");
		linkTitle.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	sessionSvc.getSession( new AsyncCallback<User>() {
                    @Override
                    public void onSuccess(User user) {
                    	if(user != null) {
                    		if(user instanceof Subscribed) {
                    			RootPanel.get("mainContent").clear();
                    			RootPanel.get("mainContent").add(getArticlePage(article));
                    		}
                    		else if(user instanceof User) {
                    			RootPanel.get("mainContent").clear();
                    			RootPanel.get("mainContent").add(user.getPaymentPage());
                    		}
                    	}else {
                    		userSvc.getRegistrationUserPage( new AsyncCallback<RegistrationUserPage>() {
                    			@Override
                    			public void onFailure(Throwable caught) {
                    				Window.alert(caught.getMessage());
                    			}
                    			@Override
                    			public void onSuccess(RegistrationUserPage result) {
            		            	RootPanel.get("mainContent").clear();
            		            	RootPanel.get("mainContent").add(result.getRegistrationUserPage());
                    			}
                    		});
                    	}
                    }
                    @Override
                    public void onFailure(Throwable caught) {
                        Window.alert(caught.getMessage());
                    }
                });
            	
            }
		});
		linkTitle.addStyleName("titleArticle");
		
		Hyperlink linkAuthor = new Hyperlink(getAuthor().getNickname(), "");
		linkAuthor.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	sessionSvc.getSession( new AsyncCallback<User>() {
                    @Override
                    public void onSuccess(User user) {
                    	if(user != null) {
                    		RootPanel.get("mainContent").clear();
                        	RootPanel.get("mainContent").add(getAuthor().getProfilePage());
                    	}else {
                    		userSvc.getRegistrationUserPage( new AsyncCallback<RegistrationUserPage>() {
                    			@Override
                    			public void onFailure(Throwable caught) {
                    				Window.alert(caught.getMessage());
                    			}
                    			@Override
                    			public void onSuccess(RegistrationUserPage result) {
            		            	RootPanel.get("mainContent").clear();
            		            	RootPanel.get("mainContent").add(result.getRegistrationUserPage());
                    			}
                    		});
                    	}
                    }
                    @Override
                    public void onFailure(Throwable caught) {
                        Window.alert(caught.getMessage());
                    }
                });
            }
		});
		linkAuthor.addStyleName("authorLink");
		
		Label contentLabel = new Label(getContent().substring(0,30) + " [....]");
		
		articlePanel.add(linkTitle);
		articlePanel.add(linkAuthor);
		articlePanel.add(contentLabel);
		Label likesLabel = new Label(article.getLikes().size() + " Mi Piace");
		articlePanel.add(likesLabel);
		likesLabel.addStyleName("like");
		
		Date time = new Date((long)article.getDateCreation()*1000);
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy HH:mm:ss");
		Label dateTimeLabel = new Label(dateFormat.format(time));
		dateTimeLabel.addStyleName("labelDate");
		articlePanel.add(dateTimeLabel);
		
		HTML html = new HTML("<hr  style=\"width:100%;\" />");
		articlePanel.add(html);
		
		return articlePanel;
	}
}