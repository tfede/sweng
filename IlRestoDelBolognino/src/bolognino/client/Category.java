package bolognino.client;

import java.io.Serializable;
import java.util.ArrayList;

public class Category implements Serializable{
	private String category; //KEY
	private ArrayList<String> subCategory;

	public Category() {
		
	}
	
	public Category(String category, ArrayList<String> subCategory){
		this.category = category;
		this.subCategory = subCategory;
	}

	public String getCategory(){
		return category;
	}
	
	public ArrayList<String> getSubCategory(){
		return subCategory;
	}
	
	public void addSubCategory(String subCategory) {
		if(this.subCategory.indexOf(subCategory) == -1) {
			this.subCategory.add(subCategory);
		}
	}
	
	public void removeSubCategory(String subCategory) {
		if(this.subCategory.indexOf(subCategory) != -1) {
			this.subCategory.remove(subCategory);
		}
	}
	
	public String printSubCategory(ArrayList<String> subCategory){
		String sub = "";
		for(int i=0; i<subCategory.size(); i++){
			sub += subCategory.get(i) + " ";
		}
		return sub;
	}
}
