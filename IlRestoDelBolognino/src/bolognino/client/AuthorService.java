package bolognino.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


@RemoteServiceRelativePath("author")
public interface AuthorService extends RemoteService {
	
	Boolean newArticle(Author author, String title, String content, String category, String subCategory);
	
	Boolean newArticlePremium(Author author, String title, String content, String category, String subCategory);
	
	ArrayList<Article> getArticlesByAuthor(String nicknameAuthor);
	
	Article getArticleById(int idArticles);
	
	Boolean editArticle(Article article);
	
	Boolean deleteArticle(int idArticle);

}
