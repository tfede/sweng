package bolognino.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Author extends Subscribed implements Serializable {
	
	private Date dateRegistration;
	
	public Author(){
		
	}
	
	public Author(String nickname, String password, String email, String firstname, String surname, Date birthday, String state, String province, String region, String city, String address, Boolean gender, String birthPlace, ArrayList<String> categories, ArrayList<String> likesAndComments) {
		super(nickname, password, email, firstname, surname, birthday, state, province, region, city, address, gender, birthPlace, categories, likesAndComments, "", "", "", null, "", "", "");
		
		this.dateRegistration = new Date();	
	}

	public Date getDateRegistration() {
		return dateRegistration;
	}

	//articoli premium con possobilit� di commento... + getProfilePage() + getCreationAriclePage() + getManageArticlePage()
	@Override
	public VerticalPanel getHomePage() { //ritorna la pagina principale per un utente autore
		VerticalPanel mainPanel = new VerticalPanel();
		HorizontalPanel buttonsPanel = new HorizontalPanel();
		Button profilePageButton = new Button("Visualizza il tuo profilo");
		profilePageButton.addStyleName("btn");
		Button createArticleButton = new Button("Crea un articolo");
		createArticleButton.addStyleName("btn");
		Button logoutButton = new Button("Logout");
		logoutButton.addStyleName("btn");

		profilePageButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	RootPanel.get("mainContent").clear();
	        	RootPanel.get("mainContent").add(getProfilePage());
	        }
	    });
		
		createArticleButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	RootPanel.get("mainContent").clear(); 						        	
	        	RootPanel.get("mainContent").add(getCreationAritclePage());
	        }
	    });
		
		logoutButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	UserServiceAsync userSvc = GWT.create(UserService.class);
	        	userSvc.getHomePage( new AsyncCallback<HomePage>() {
        			@Override
        			public void onFailure(Throwable caught) {
        				Window.alert(caught.getMessage());
        			}
        			@Override
        			public void onSuccess(HomePage result) {
        				MySessionServiceAsync sessionSvc = GWT.create(MySessionService.class);
        		        sessionSvc.exitSession( new AsyncCallback<Void>() {
        		            @Override
        		            public void onSuccess(Void result2) {
        		            	RootPanel.get("mainContent").clear();
                				RootPanel.get("mainContent").add(result.getHomePage());
        		            }
        		            @Override
        		            public void onFailure(Throwable caught) {
        		                Window.alert(caught.getMessage());
        		            }
        		        });
        			}
        		});
	        }
	    });


		Label welcomeLabel = new Label("Bentornato " + getNickname());
		welcomeLabel.addStyleName("welcome");
		mainPanel.add(welcomeLabel);
		buttonsPanel.add(profilePageButton);
		buttonsPanel.add(createArticleButton);
		buttonsPanel.add(logoutButton);
		mainPanel.add(buttonsPanel);
		mainPanel.add(getArticles(new ArrayList<String>()));
		
		return mainPanel;
	}
	
	//getManageArticlePage() + getCreationArticlePage() 
	@Override
	public VerticalPanel getProfilePage() { //ritorna la pagina principale per un utente autore
		VerticalPanel mainPanel = new VerticalPanel();
		UserServiceAsync userSvc = GWT.create(UserService.class);
		MySessionServiceAsync sessionSvc = GWT.create(MySessionService.class);
		
		Button homePageButton = new Button("Torna indietro");
		homePageButton.addStyleName("btn");
		
        sessionSvc.getSession( new AsyncCallback<User>() {
            @Override
            public void onSuccess(User user) {
            	//PROPRIETARIO DELLA PAGINA PROFILO
            	if(user != null) {
            		homePageButton.addClickHandler(new ClickHandler() {
                        public void onClick(ClickEvent event) {
                        	RootPanel.get("mainContent").clear();
                        	RootPanel.get("mainContent").add(user.getHomePage());
                        }
            		});
            	} 
            }
            @Override
            public void onFailure(Throwable caught) {
                Window.alert(caught.getMessage());
            }
        });
		
		

		
		
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		String dateRegistrationAuthor = dateFormat.format(getDateRegistration());
		Label welcome = new Label("Profilo di: " + getFirstname() + "  " + getSurname() + ", creato il: " + dateRegistrationAuthor);
		welcome.addStyleName("welcome");
		mainPanel.add(welcome);
		mainPanel.add(homePageButton);
		mainPanel.add(getYourArticlePage());

		return mainPanel;
	}
	
	private ArrayList<Category> categories = new ArrayList<Category>();
	public VerticalPanel getCreationAritclePage(){
		AuthorServiceAsync authorSvc = GWT.create(AuthorService.class);
		UserServiceAsync userSvc = GWT.create(UserService.class);
		VerticalPanel createArticlePanel = new VerticalPanel();
		Button homePageButton = new Button("Torna indietro");
		homePageButton.addStyleName("btn");
		
		homePageButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	RootPanel.get("mainContent").clear();
            	RootPanel.get("mainContent").add(getHomePage());
            }
		});
		createArticlePanel.add(homePageButton);
		
    	Label titleArticleLabel = new Label("Titolo dell'articolo *:");
    	titleArticleLabel.addStyleName("label");
    	TextBox titleArticleInput = new TextBox();
    	titleArticleInput.addStyleName("titleArticleTextArea");
    	
    	Label contentArticleLabel = new Label("Contenuto *:");
    	contentArticleLabel.addStyleName("label");
    	TextArea contentArticleInput = new TextArea();
    	contentArticleInput.addStyleName("contentArticleTextArea");
    	
    	RadioButton articleTypeFree = new RadioButton("myRadioGroup", "Free");
		RadioButton articleTypePremium = new RadioButton("myRadioGroup", "Premium");
		
		
		Label categoryLabel = new Label("Scegli la categoria *:");
		categoryLabel.addStyleName("label");
		ListBox categoryList = new ListBox();
		categoryList.addItem("Scegli la categoria");
		
		Label subCategoryLabel = new Label("Scegli la sottocategoria :");
		subCategoryLabel.addStyleName("label");
		ListBox subCategoryList = new ListBox();

		userSvc.getCategoriesArrayList(new AsyncCallback<ArrayList<Category>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Cannot load symbols: " + caught.getMessage());
				RootPanel.get("mainContent").add(getProfilePage());
			}
			@Override
			public void onSuccess(ArrayList<Category> result) {
				categories = result;
				if(result.size() != 0) {
					for(int i = 0; i < result.size(); i++) {
						categoryList.addItem(result.get(i).getCategory());
					}
				}
			}
		});	
		
		categoryList.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				subCategoryList.clear();
				subCategoryList.addItem("Scegli la sottocategoria");
				for(int i = 0; i < categories.size(); i++) {
					if(categories.get(i).getCategory().equals(categoryList.getSelectedItemText())) {
						for(int j = 0; j < categories.get(i).getSubCategory().size(); j++) {
							subCategoryList.addItem(categories.get(i).getSubCategory().get(j));
						}
					}
				}
			}
		});
		
		Button confirmCreationButton = new Button("Crea articolo");
		confirmCreationButton.addStyleName("btn");
		
		confirmCreationButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	if((articleTypeFree.getValue() || articleTypePremium.getValue()) && titleArticleInput.getText().length() > 0 && contentArticleInput.getText().length() > 0 && categoryList.getSelectedItemText().length() > 0 && !categoryList.getSelectedItemText().equals("Scegli la categoria")) {
		        	if(Window.confirm("Vuoi procedere con la creazione dell'articolo?")) {
		            	if(articleTypeFree.getValue()) {
		            		String subCategory = "";
		            		if(subCategoryList.getSelectedItemText() != null) {
			            		if(!subCategoryList.getSelectedItemText().equals("Scegli la sottocategoria")) {
			            			subCategory = subCategoryList.getSelectedItemText();
			            		}
		            		}
		            		Author author = new Author(getNickname(), getPassword(), getEmail(), getFirstname(), getSurname(), getBirthday(), getState(), getProvince(), getRegion(), getCity(), getAddress(), getGender(), getBirthPlace(), getCategories(), getLikesAndComments());
			        		authorSvc.newArticle(author, titleArticleInput.getText(), contentArticleInput.getText(), categoryList.getSelectedItemText(), subCategory, new AsyncCallback<Boolean>() {
			        			@Override
			        			public void onFailure(Throwable caught) {
			        				Window.alert("Cannot load symbols: " + caught.getMessage());
			        				RootPanel.get("mainContent").add(getProfilePage());
			        			}
			        			@Override
			        			public void onSuccess(Boolean result) {
			        				RootPanel.get("mainContent").clear();
			        				if(result) {
			        					Window.alert("Articolo creato con successo!");
			        					RootPanel.get("mainContent").add(getProfilePage());
			        				}else {
			        					Window.alert("La creazione dell'articolo non � andata a buon fine. Riprovare");
			        					RootPanel.get("mainContent").add(getProfilePage());
			        				}
			        			}
			        		});	
		            	}else {
		            		String subCategory = "";
		            		if(subCategoryList.getSelectedItemText() != null) {
			            		if(!subCategoryList.getSelectedItemText().equals("Scegli la sottocategoria")) {
			            			subCategory = subCategoryList.getSelectedItemText();
			            		}
		            		}
		            		Author author = new Author(getNickname(), getPassword(), getEmail(), getFirstname(), getSurname(), getBirthday(), getState(), getProvince(), getRegion(), getCity(), getAddress(), getGender(), getBirthPlace(), getCategories(), getLikesAndComments());
		            		authorSvc.newArticlePremium(author, titleArticleInput.getText(), contentArticleInput.getText(), categoryList.getSelectedItemText(), subCategory, new AsyncCallback<Boolean>() {
			        			@Override
			        			public void onFailure(Throwable caught) {
			        				Window.alert("Cannot load symbols: " + caught.getMessage());
			        				RootPanel.get("mainContent").add(getProfilePage());
			        			}
			        			@Override
			        			public void onSuccess(Boolean result) {
			        				RootPanel.get("mainContent").clear();
			        				if(result) {
			        					Window.alert("Articolo creato con successo!");
			        					RootPanel.get("mainContent").add(getProfilePage());
			        				}else {
			        					Window.alert("La creazione dell'articolo non � andata a buon fine. Riprovare");
			        					RootPanel.get("mainContent").add(getProfilePage());
			        				}
			        			}
			        		});
		            	}
		        	}
	        	} else {
	        		Window.alert("Compila tutti i campi obbligatori!");
	        	}
	        }
	    });
		
		createArticlePanel.add(titleArticleLabel);
		createArticlePanel.add(titleArticleInput);
		createArticlePanel.add(contentArticleLabel);
		createArticlePanel.add(contentArticleInput);
		createArticlePanel.add(articleTypeFree);
		createArticlePanel.add(articleTypePremium);
		createArticlePanel.add(categoryLabel);
		createArticlePanel.add(categoryList);
		createArticlePanel.add(subCategoryLabel);
		createArticlePanel.add(subCategoryList);
		
		createArticlePanel.add(confirmCreationButton);
		
		return createArticlePanel;
	}
	
	public VerticalPanel getYourArticlePage() {
		VerticalPanel yourArticlePanel = new VerticalPanel();
		
		AuthorServiceAsync authorSvc = GWT.create(AuthorService.class);
    	authorSvc.getArticlesByAuthor(getNickname(), new AsyncCallback<ArrayList<Article> >() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
				RootPanel.get("mainContent").add(getProfilePage());
			}
			@Override
			public void onSuccess(ArrayList<Article>  result) {
				if(result.size() > 0) {
					for(int i = result.size() - 1; i >= 0; i--) {
						Label titleArticle = new Label(result.get(i).getTitle());
						titleArticle.addStyleName("titleArticle");
						Label contentArticle = new Label(result.get(i).getContent());
						contentArticle.addStyleName("contentArticle");
						
						yourArticlePanel.add(result.get(i).getArticlePreview(result.get(i)));
					}
				}else {
					Label error = new Label("Non hai creato ancora nessun articolo");
					error.addStyleName("label");
					RootPanel.get("mainContent").add(error);
				}
			}
		});
    	return yourArticlePanel;
	}
}