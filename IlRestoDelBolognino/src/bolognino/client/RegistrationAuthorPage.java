package bolognino.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DatePicker;

public class RegistrationAuthorPage implements Serializable{
	public RegistrationAuthorPage() {
		
	}
	public VerticalPanel getRegistrationAuthorPage() {
		UserServiceAsync userSvc = GWT.create(UserService.class);

		VerticalPanel mainPanel = new VerticalPanel();
		Button getHomePageButton = new Button("Torna alla home");
		getHomePageButton.addStyleName("btn");

		//FormPanel newUserForm = new FormPanel();
		
		Label nicknameLabel = new Label("Nickname *:");
		nicknameLabel.addStyleName("label");
		TextBox nicknameInput = new TextBox();

		Label passwordLabel = new Label("Password *:");
		passwordLabel.addStyleName("label");
		PasswordTextBox passwordInput = new PasswordTextBox();

		Label emailLabel = new Label("E-Mail *:");
		emailLabel.addStyleName("label");
		TextBox emailInput = new TextBox();
		
		Label firstnameLabel = new Label("Nome *:");
		firstnameLabel.addStyleName("label");
		TextBox firstnameInput = new TextBox();
		
		Label surnameLabel = new Label("Cognome *:");
		surnameLabel.addStyleName("label");
		TextBox surnameInput = new TextBox();
		
		Label birthdayLabel = new Label("Data di nascita *:");
		birthdayLabel.addStyleName("label");
		DatePicker birthdayInput = new DatePicker();
		
		Label stateLabel = new Label("Stato *:");
		stateLabel.addStyleName("label");
		TextBox stateInput = new TextBox();
		
		Label provinceLabel = new Label("Provincia *:");
		provinceLabel.addStyleName("label");
		TextBox provinceInput = new TextBox();
		
		Label regionLabel = new Label("Regione: *");
		regionLabel.addStyleName("label");
		TextBox regionInput = new TextBox();
		
		Label cityLabel = new Label("Citta' *:");
		cityLabel.addStyleName("label");
		TextBox cityInput = new TextBox();
		
		Label addressLabel = new Label("Indirizzo *:");
		addressLabel.addStyleName("label");
		TextBox addressInput = new TextBox();
		
		Label genderLabel = new Label("Genere:");
		genderLabel.addStyleName("label");
		RadioButton genderInputM = new RadioButton("myRadioGroup", "Maschio");
		RadioButton genderInputF = new RadioButton("myRadioGroup", "Femmina");
		
		Label birthPlaceLabel = new Label("Luogo di nascita");
		birthPlaceLabel.addStyleName("label");
		TextBox birthPlaceInput = new TextBox();
		
		Button newAuthorButton = new Button("Crea il tuo profilo!");
		newAuthorButton.addStyleName("btn");
		
		mainPanel.add(getHomePageButton);
		mainPanel.add(nicknameLabel);
		//nicknameInput.addStyleName("formInput");
		mainPanel.add(nicknameInput);
		mainPanel.add(passwordLabel);
		//passwordInput.addStyleName("formInput");
		mainPanel.add(passwordInput);
		mainPanel.add(emailLabel);
		//emailInput.addStyleName("formInput");
		mainPanel.add(emailInput);
		mainPanel.add(firstnameLabel);
		//firstnameInput.addStyleName("formInput");
		mainPanel.add(firstnameInput);
		mainPanel.add(surnameLabel);
		//surnameInput.addStyleName("formInput");
		mainPanel.add(surnameInput);
		mainPanel.add(birthdayLabel);
		mainPanel.add(birthdayInput);
		mainPanel.add(stateLabel);
		//stateInput.addStyleName("formInput");
		mainPanel.add(stateInput);
		mainPanel.add(provinceLabel);
		//provinceInput.addStyleName("formInput");
		mainPanel.add(provinceInput);
		mainPanel.add(regionLabel);
		//regionInput.addStyleName("formInput");
		mainPanel.add(regionInput);
		mainPanel.add(cityLabel);
		//cityInput.addStyleName("formInput");
		mainPanel.add(cityInput);
		mainPanel.add(addressLabel);
		//addressInput.addStyleName("formInput");
		mainPanel.add(addressInput);
		mainPanel.add(genderLabel);
		mainPanel.add(genderInputM);
		mainPanel.add(genderInputF);
		mainPanel.add(birthPlaceLabel);
		mainPanel.add(birthPlaceInput);

		mainPanel.add(newAuthorButton);
		
		newAuthorButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	String nickname = nicknameInput.getText();
            	String password = passwordInput.getText();
            	String email = emailInput.getText();
            	String firstname = firstnameInput.getText();
            	String surname = surnameInput.getText();
            	Date birthday = birthdayInput.getValue();
            	String state = stateInput.getText();
            	String province = provinceInput.getText();
            	String region = regionInput.getText();
            	String city = cityInput.getText();
            	String address = addressInput.getText();
            	String birthPlace = birthPlaceInput.getText();
            	
            	Boolean gender = null;
            	if(genderInputM.getValue()) {
            		gender = false;
            	}else if(genderInputF.getValue()){
            		gender = true;
            	}

            	if(nicknameInput.getText().length() > 0 && passwordInput.getText().length() > 0 && emailInput.getText().length() > 0 && firstnameInput.getText().length() > 0 && surnameInput.getText().length() > 0 && birthdayInput.getValue() != null && stateInput.getText().length() > 0 && provinceInput.getText().length() > 0 && regionInput.getText().length() > 0 && cityInput.getText().length() > 0 && addressInput.getText().length() > 0) {
					userSvc.newAuthor(nickname, password, email, firstname, surname, birthday, state, province, region, city, address, gender, birthPlace, new ArrayList<String>(), new ArrayList<Article>() ,new AsyncCallback<Boolean>() {
						@Override
						public void onFailure(Throwable caught) {
							Window.alert(caught.getMessage());								
						}
						@Override
						public void onSuccess(Boolean result) {
							if(result) {
								userSvc.getLoginPage( new AsyncCallback<LoginPage>() {
									@Override
									public void onFailure(Throwable caught) {
										Window.alert(caught.getMessage());
									}
									@Override
									public void onSuccess(LoginPage result) {
										RootPanel.get("mainContent").clear();
										RootPanel.get("mainContent").add(result.getLoginPage());
									}
								});
							}else {
								Window.alert("Nickname e/o Email gia' utilizzata");
							}
						}
					});
            	} else {
            		Window.alert("Compila tutti i campi obbligatori!");
            	}
            }
		});
		
		getHomePageButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	userSvc.getHomePage( new AsyncCallback<HomePage>() {
        			@Override
        			public void onFailure(Throwable caught) {
        				//Window.alert("Cannot load symbols: " + caught.getMessage());
        			}
        			@Override
        			public void onSuccess(HomePage result) {
        				MySessionServiceAsync sessionSvc = GWT.create(MySessionService.class);
        		        sessionSvc.getSession( new AsyncCallback<User>() {
        		            @Override
        		            public void onSuccess(User user) {
        		            	RootPanel.get("mainContent").clear();
                				RootPanel.get("mainContent").add(result.getHomePage());
        		            }
        		            @Override
        		            public void onFailure(Throwable caught) {
        		                Window.alert(caught.getMessage());
        		            }
        		        });
        			}
        		});
            }
		});
		
		return mainPanel;
	}
}
