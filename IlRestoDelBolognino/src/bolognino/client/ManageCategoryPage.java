package bolognino.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.gargoylesoftware.htmlunit.javascript.host.Console;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/*
class Row {
	public String category;
	public ArrayList<String> subCategory;
	
	public Row(String category, ArrayList<String> subCategory) {
		this.category = category;
		this.subCategory = subCategory;
	}
}*/

public class ManageCategoryPage implements Serializable {
	public ManageCategoryPage() {
		
	}

	private Integer id = 0;
	private ArrayList<String> tableRows = new ArrayList<>();
	private Map<Integer, Row> categoriesMap = new HashMap<Integer, Row>();

	public VerticalPanel getManageCategoryPage() {
		AdminServiceAsync adminSvc = GWT.create(AdminService.class);
	    VerticalPanel mainPanel = new VerticalPanel();
	    FlexTable table = new FlexTable();
	    HorizontalPanel footerButtons = new HorizontalPanel();
		Button addCategoryButton = new Button("Aggiungi categoria");
		Button saveButton = new Button("Applica le modifiche");

	    adminSvc.getCategoriesList(new AsyncCallback<ArrayList<Category>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Cannot load symbols: " + caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Category>  result) {
				/*for(int i = 0; i < result.size(); i++) {
					ArrayList<String> subcategories = new ArrayList<>();
					GWT.log("Categoria " + result.get(i).get(0));

					for(int j = 0; j < result.get(i).size(); j++) {
						if(!result.get(i).get(0).equals(result.get(i).get(j))) {
							GWT.log("Sottocategoria " + result.get(i).get(j));
							subcategories.add(result.get(i).get(j));
						}
					}
					
					addCategory(table, result.get(i).get(0), subcategories);
				}*/
				
				for(int i = 0 ; i < result.size(); i++) {
					addCategory(table, result.get(i).getCategory(), result.get(i).getSubCategory());
				}
				
				/*for (Map.Entry<String,ArrayList<String>> entry : result.entrySet()) {
					addCategory(table, entry.getKey(), entry.getValue());
				}*/
				
				/*for(int i = 0; i < result.size(); i++) {
					result.get(i).get(0);

					if(result.get(i).size() > 1) {
						ArrayList<String> subcategories = result.get(i);
						subcategories.remove(0);
						addCategory(table, result.get(i).get(0), subcategories);
						
						GWT.log("Categoria " + result.get(i).get(0));
						for(int j = 0; j < subcategories.size(); i++) {
							GWT.log("Sottocategoria " + subcategories.get(i));
						}
					} else {
						addCategory(table, result.get(i).get(0), new ArrayList<String>());
						
						GWT.log("Categoria " + result.get(i).get(0));
					}
					//addCategory(table, result.get(i));
				}*/
			}
		});

		addCategoryButton.addClickHandler(new ClickHandler() {
		    public void onClick(ClickEvent event) {
		    	boolean anotherVoidCategory = false;
		    	for(Map.Entry<Integer,Row> entry : categoriesMap.entrySet()) {
					if(entry.getValue().equals("")) {
						anotherVoidCategory = true;
					}
				}

		    	if(!anotherVoidCategory) {
		    		//ArrayList<String> categoryList = new ArrayList<>();
		    		//categoryList.add("");
			    	addCategory(table, "", new ArrayList<>());
		    	} else {
		    		Window.alert("Hai una categoria senza nome!");
		    	}
	        }
	    });
		
		saveButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	boolean printError = false;
	        	for(Map.Entry<Integer,Row> entry : categoriesMap.entrySet()) {
	        		if(entry.getValue().category.equals("") && !printError) {
	        			Window.alert("C'� una categoria senza nome, non verr� considerata.");
	        			printError = true;
	        		}
	        		/*Window.alert(entry.getValue().category);
	        		for(int i = 0; i < entry.getValue().subCategory.size(); i++) {
	        			Window.alert(entry.getValue().subCategory.get(i));
	        		}*/
				}

	        	Map<String, ArrayList<String>> newCategories = new HashMap<String, ArrayList<String>>();
	        	for(Map.Entry<Integer,Row> entry : categoriesMap.entrySet()) {
	        		if(!entry.getValue().category.equals("")) {
	        			//ArrayList<String> subcategories = entry.getValue().subCategory;
	        			//if(subcategories.get(subcategories.size()-1).equals(""))
	        				//subcategories.remove(subcategories.size()-1);
	        			newCategories.put(entry.getValue().category, entry.getValue().subCategory);
	        		}
				}

	        	adminSvc.replaceCategories(newCategories, new AsyncCallback<Boolean>() {
        			@Override
        			public void onFailure(Throwable caught) {
        				Window.alert("Cannot load symbols: " + caught.getMessage());
        			}
        			@Override
        			public void onSuccess(Boolean result) {
        				if(result) {
        					Window.alert("Categorie aggiornate con successo!");
        					
        					for(Map.Entry<String,ArrayList<String>> entry : newCategories.entrySet()) {
            					Window.alert("CAT " + entry.getKey());
            					for(int i = 0; i < entry.getValue().size(); i++) {
            	        			Window.alert("SUB " + entry.getValue().get(i));
            	        		}
            				}
        				}
        			}
        		});
	        }
	    });

	    table.setText(0, 0, "Categoria");
		table.setText(0, 1, "Sottocategoria");
		table.setText(0, 2, "Gestisci Sottocategorie");
		table.setText(0, 3, "Elimina Categoria");

		footerButtons.add(addCategoryButton);
		footerButtons.add(saveButton);

		mainPanel.add(table);
		mainPanel.add(footerButtons);

		return mainPanel;
	}

	public void addCategory(FlexTable table, String category, ArrayList<String> subcategories) {
		final Integer innerId = id;
		tableRows.add(innerId.toString());

		TextBox categoryTextBox = new TextBox();
		VerticalPanel subCategory = new VerticalPanel();
		ListBox subCategoryListBox = new ListBox();
		TextBox selectedSubcategoryTextBox = new TextBox();
		HorizontalPanel editSubcategory = new HorizontalPanel();
		Button addSubcategoryButton = new Button("+");
		Button removeSubcategoryButton = new Button("x");
		Button removeCategoryButton = new Button("x");
		
		categoriesMap.put(innerId, new Row(category, subcategories));

    	categoryTextBox.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				boolean anotherOneEqual = false; //categorie uguali
				ArrayList<String> allCategories = new ArrayList<String>(); 
				
				for(Map.Entry<Integer,Row> entry : categoriesMap.entrySet()) {
					allCategories.add(entry.getValue().category);
				}

				allCategories.remove((allCategories.indexOf("")));

				if(allCategories.indexOf(categoryTextBox.getText()) != -1) {	//se ci sono altre categorie uguali
					anotherOneEqual = true;
				}

				//Controlliamo che non ci sia un'altra categoria uguale o che la textBox non sia vuota
				if(categoryTextBox.getText().equals("") || anotherOneEqual) {
					categoryTextBox.setText(categoriesMap.get(innerId).category);
				} else { //Se l'inserimento pu� essere fatto
					categoriesMap.put(innerId, new Row(categoryTextBox.getText(), categoriesMap.get(innerId).subCategory));
				}
			}
		});

		selectedSubcategoryTextBox.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				if(!subCategoryListBox.getSelectedValue().equals("Scegli la sottocategoria") && !selectedSubcategoryTextBox.getText().equals("Scegli la sottocategoria")) {
					boolean anotherOneEqual = false; //sottocategorie uguali
					ArrayList<String> allSubcategories = new ArrayList<String>(); 
					
					for(Map.Entry<Integer,Row> entry : categoriesMap.entrySet()) {
						for(int i = 0; i < entry.getValue().subCategory.size(); i++) {
							allSubcategories.add(entry.getValue().subCategory.get(i));
						}
					}

					allSubcategories.remove((allSubcategories.indexOf("")));

					if(allSubcategories.indexOf(selectedSubcategoryTextBox.getText()) == -1) {	//se non altre categorie uguali
						subCategoryListBox.setItemText(subCategoryListBox.getSelectedIndex(), selectedSubcategoryTextBox.getText());
						ArrayList<String> subcategories = categoriesMap.get(innerId).subCategory;
						subcategories.set(subcategories.indexOf(""), selectedSubcategoryTextBox.getText());
						categoriesMap.put(innerId, new Row(categoryTextBox.getText(), subcategories));
					} else {
						Window.alert("Questa sottocategoria risulta gi� presente fra le sottocategorie.");
						selectedSubcategoryTextBox.setText("");
					}
				} else if(selectedSubcategoryTextBox.getText().equals("Scegli la sottocategoria")) {
					Window.alert("Non puoi inserire questa categoria!");
					selectedSubcategoryTextBox.setText("");
				}
			}
		});

		subCategoryListBox.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				int index = subCategoryListBox.getSelectedIndex(); 

				if(!subCategoryListBox.getItemText(index).equals("Scegli la sottocategoria"))
					selectedSubcategoryTextBox.setText(subCategoryListBox.getItemText(index));
				else
					selectedSubcategoryTextBox.setText("");
			}
		});

		addSubcategoryButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	if(categoriesMap.get(innerId).subCategory.indexOf("") == -1) {	//Se non esiste una sottocategoria vuota
	        		ArrayList<String> subcategories = categoriesMap.get(innerId).subCategory;
	        		subcategories.add("");
	        		selectedSubcategoryTextBox.setText("");
		        	subCategoryListBox.addItem("");
		        	subCategoryListBox.setItemSelected(subCategoryListBox.getItemCount()-1, true);
	        	}
	        }
	    });

		removeSubcategoryButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	if(subCategoryListBox.getSelectedItemText() != "Scegli la sottocategoria") {
	        		subCategoryListBox.removeItem(subCategoryListBox.getSelectedIndex());
	        		selectedSubcategoryTextBox.setText("");

	        		ArrayList<String> subcategories = categoriesMap.get(innerId).subCategory;
	        		subcategories.remove(subcategories.indexOf(selectedSubcategoryTextBox.getText()));
	        		categoriesMap.put(innerId, new Row(categoryTextBox.getText(), subcategories));
	        	}
	        }
	    });

		removeCategoryButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	int removedIndex = tableRows.indexOf(removeCategoryButton.getElement().getId());
	        	tableRows.remove(removedIndex);
	            table.removeRow(removedIndex + 1);

	            categoriesMap.remove(innerId);
	        }
	    });

		//Lato front-end
		categoryTextBox.setText(category);
		categoryTextBox.getElement().setId((innerId).toString());

    	subCategoryListBox.addItem("Scegli la sottocategoria");
    	
    	for(int i = 0; i < subcategories.size(); i++) {
			subCategoryListBox.addItem(subcategories.get(i));
		}

		subCategory.add(subCategoryListBox);
		subCategory.add(selectedSubcategoryTextBox);

		editSubcategory.add(addSubcategoryButton);
		editSubcategory.add(removeSubcategoryButton);

		int row = table.getRowCount();
		removeCategoryButton.getElement().setId(((Integer) (innerId)).toString()); //Quando elimino la riga devo sapere che riga eliminare dalla tabella
		table.setWidget(row, 0, categoryTextBox);			//name category
		table.setWidget(row, 1, subCategory);				// list subcategory
		table.setWidget(row, 2, editSubcategory);			// button manage subcategory
		table.setWidget(row, 3, removeCategoryButton);		// remove category

		id++;
	}
}