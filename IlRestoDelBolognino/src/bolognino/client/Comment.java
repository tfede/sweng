package bolognino.client;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable{
	private Integer id;
	private String nicknameUser;
	private String comment;
	private long dateCreation;
	
	public Comment() {
	
	}
	
	public Comment(Integer id, String nicknameUser, String comment) {
		this.id = id;
		this.nicknameUser = nicknameUser;
		this.comment = comment;
		Date now = new Date();
		this.dateCreation = now.getTime() / 1000L;
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getNicknameUser() {
		return nicknameUser;
	}
	
	public String getComment() {
		return comment;
	}
	
	public long getDateCreation() {
		return dateCreation;
	}
}
