package bolognino.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("users")
public interface UserService extends RemoteService {
	//User getRegisterPage();
	
	RegistrationUserPage getRegistrationUserPage();
	
	AccessRegistrationAuthorPage getAccessRegistrationAuthorPage();
	
	RegistrationAuthorPage getRegistrationAuthorPage();
	
	HomePage getHomePage();
	
	LoginPage getLoginPage();
	
	Boolean newUser(String nickname, String password, String email, String firstname, String surname, Date birthday, String state, String province, String region, String city, String address, Boolean gender, String birthPlace, ArrayList<String> categories, ArrayList<String> likesAndComments);
	
	Boolean newAuthor(String nickname, String password, String email, String firstname, String surname, Date birthday, String state, String province, String region, String city, String address, Boolean gender, String birthPlace, ArrayList<String> categories, ArrayList<String> likesAndComments);
	
	//Boolean newAdmin();
	
	User upgradeUser(User user, String ncc, String cardHolderName, String cardHolderSurname, Date cardDeadline, String cvv, String nation, String cap);
	
	User downgradeUser(User user);
	
	User getAuthentication(String nickname, String password);
	
	Boolean checkAuthenticationToken(String token);
	
	String[] getCategoriesArray();
	
	ArrayList<Category> getCategoriesArrayList();
	
	ArrayList<Article> getArticlesForUser(ArrayList<String> subCategories);
	
	Boolean postComment(Article article, String userNickname, String commentText);
	
	Boolean likeArticle(String nicknameUser, Article article);
	
	ArrayList<String> getArticlesLikedAndCommentedByUser(String nicknameUser);

	Article getArticleById(int idArticles);
	
	User getUserByNickname(String nicknameUser);
}
