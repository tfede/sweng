package bolognino.client;

import java.io.Serializable;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class AccessRegistrationAuthorPage implements Serializable {
	
	public AccessRegistrationAuthorPage() {
		
	}
	public VerticalPanel getAccessRegistrationAuthorPage() {
		UserServiceAsync userSvc = GWT.create(UserService.class);
		VerticalPanel mainPanel = new VerticalPanel();
		Button getHomePageButton = new Button("Torna alla home");
		getHomePageButton.addStyleName("btn");
		Label tokenLabel = new Label("Inserimento token:");
		tokenLabel.addStyleName("label");
		PasswordTextBox tokenInput = new PasswordTextBox();
		Label error = new Label("Token errato");
		Button loginButton = new Button("Accedi all'area");
		loginButton.addStyleName("btn");
		
		mainPanel.add(getHomePageButton);
		mainPanel.add(tokenLabel);
		mainPanel.add(tokenInput);
		mainPanel.add(loginButton);
		
		
		getHomePageButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	userSvc.getHomePage( new AsyncCallback<HomePage>() {
        			@Override
        			public void onFailure(Throwable caught) {
        				Window.alert("Cannot load symbols: " + caught.getMessage());
        			}
        			@Override
        			public void onSuccess(HomePage result) {
        				RootPanel.get("mainContent").clear();
        				RootPanel.get("mainContent").add(result.getHomePage());
        			}
        		});
            }
		});
		
		loginButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	String token = tokenInput.getText();
            	userSvc.checkAuthenticationToken(token, new AsyncCallback<Boolean>() {
        			@Override
        			public void onFailure(Throwable caught) {
        				Window.alert("Cannot load symbols: " + caught.getMessage());
        			}
        			@Override
        			public void onSuccess(Boolean result) {
        				if(!result) {
        					RootPanel.get("mainContent").add(error);
        				}
        				else {		//Token corretto --> reindirizzamento alla registrazione dell'autore
        					userSvc.getRegistrationAuthorPage(new AsyncCallback<RegistrationAuthorPage>() {
        						@Override
        						public void onFailure(Throwable caught) {
        							Window.alert("Cannot load symbols: " + caught.getMessage());
        						}
        						@Override
        						public void onSuccess(RegistrationAuthorPage result) {
        							RootPanel.get("mainContent").clear();
        							RootPanel.get("mainContent").add(result.getRegistrationAuthorPage());
        						}
        					});
        				}
        			}
        		});
            }
		});
		
		return mainPanel;
	}
}


