package bolognino.client;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Article implements Serializable {
	private Integer id;
	private Author author;
	private String title;
	private String content;
	private long dateCreation;
	private ArrayList<String> likes;			//list di nickname degli utenti che hanno messo mi piace
	private String category;
	private String subCategory;
	private ArrayList<Comment> comments;
	private ArrayList<Comment> moderatedComments;
	private ArrayList<Comment> declinedComments;
	
	public Article() {
		
	}
	
	public Article(Integer id, Author author, String title, String content, String category, String subCategory, long now, ArrayList<String> likes, ArrayList<Comment> comments, ArrayList<Comment> moderatedComments, ArrayList<Comment> declinedComments) {
		this.id = id;
		this.author = author;
		this.title = title;
		this.content = content;
		this.category = category;
		this.subCategory = subCategory;
		this.dateCreation = now;
		this.likes = likes;
		this.comments = comments;
		this.moderatedComments = moderatedComments; 
		this.declinedComments = declinedComments; 
	}
	
	public Integer getId() {
		return id;
	}
	
	public Author getAuthor() {
		return author;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getContent() {
		return content;
	}
	
	public long getDateCreation() {
		return dateCreation;
	}
	
	public ArrayList<String> getLikes() {
		return likes;
	}
	
	public String getCategory(){
		return category;
	}
	
	public String getSubCategory(){
		return subCategory;
	}
	
	public ArrayList<Comment> getComments(){
		return comments;
	}
	
	public ArrayList<Comment> getModeratedComments(){
		return moderatedComments;
	}
	
	public ArrayList<Comment> getDeclinedComments(){
		return declinedComments;
	}
	
	public void addComment(Comment comment) {
		this.comments.add(comment);
	}
	
	public VerticalPanel getArticlePreview(Article article) {
		MySessionServiceAsync sessionSvc = GWT.create(MySessionService.class);
		UserServiceAsync userSvc = GWT.create(UserService.class);
		VerticalPanel articlePanel = new VerticalPanel();  
		
		Hyperlink linkTitle = new Hyperlink(getTitle(), "");
		linkTitle.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	RootPanel.get("mainContent").clear();
            	RootPanel.get("mainContent").add(getArticlePage(article));
            }
		});
		linkTitle.addStyleName("titleArticle");

		Hyperlink linkAuthor = new Hyperlink(getAuthor().getNickname(), "");
		linkAuthor.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	sessionSvc.getSession( new AsyncCallback<User>() {
                    @Override
                    public void onSuccess(User user) {
                    	if(user != null) {
                    		RootPanel.get("mainContent").clear();
                        	RootPanel.get("mainContent").add(getAuthor().getProfilePage());
                    	}else {
                    		userSvc.getRegistrationUserPage( new AsyncCallback<RegistrationUserPage>() {
                    			@Override
                    			public void onFailure(Throwable caught) {
                    				Window.alert(caught.getMessage());
                    			}
                    			@Override
                    			public void onSuccess(RegistrationUserPage result) {
            		            	RootPanel.get("mainContent").clear();
            		            	RootPanel.get("mainContent").add(result.getRegistrationUserPage());
                    			}
                    		});
                    	}
                    }
                    @Override
                    public void onFailure(Throwable caught) {
                        Window.alert(caught.getMessage());
                    }
                });
            }
		});
		linkAuthor.addStyleName("authorLink");
		
		Label contentLabel = new Label(getContent().substring(0,30) + " [....]");
		
		articlePanel.add(linkTitle);
		articlePanel.add(linkAuthor);
		articlePanel.add(contentLabel);
		Label likesLabel = new Label(article.getLikes().size() + " Mi Piace");
		likesLabel.addStyleName("like");
		articlePanel.add(likesLabel);
		
		Date time = new Date((long)article.getDateCreation()*1000);
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy HH:mm:ss");
		Label dateTimeLabel = new Label(dateFormat.format(time));
		dateTimeLabel.addStyleName("labelDate");
		articlePanel.add(dateTimeLabel);
		
		
		HTML html = new HTML("<hr  style=\"width:100%;\" />");
		articlePanel.add(html);
		return articlePanel;
	}
	
	private Integer idRow = 0;

	public VerticalPanel getArticlePage(Article article) {
		UserServiceAsync userSvc = GWT.create(UserService.class);
		MySessionServiceAsync sessionSvc = GWT.create(MySessionService.class);
		AuthorServiceAsync authorSvc = GWT.create(AuthorService.class);
		
		//For All
		VerticalPanel articlePanel = new VerticalPanel();
		Button getHomePageButton = new Button("Torna indietro");
		getHomePageButton.addStyleName("btn");
		TextArea contentCommentNew = new TextArea();
		contentCommentNew.getElement().setPropertyString("placeholder", "Inserisci commento");
		contentCommentNew.addStyleName("contentCommentTextArea");
		Button postComment = new Button("Pubblica commento");
		postComment.addStyleName("btn");
		
		//For User
		Label titleLabel = new Label(article.getTitle());
		titleLabel.addStyleName("titleArticle");
		Label contentLabel = new Label(article.getContent());
		contentLabel.addStyleName("contentArticle");
		Button likeButton = new Button("Mi piace");
		likeButton.addStyleName("btn");
		
		//For Admin and Author
		TextArea titleInput= new TextArea();
		titleInput.setText(article.getTitle());
		titleInput.addStyleName("titleArticleTextArea");
		TextArea contentInput = new TextArea();
		contentInput.setText(article.getContent());
		contentInput.addStyleName("contentArticleTextArea");
		
		sessionSvc.getSession(new AsyncCallback<User>() {
            @Override
            public void onSuccess(User user) {
        		if(user != null && article.getLikes().indexOf(user.getNickname()) != -1) {
        			likeButton.setText("Non mi piace piu'");
            	}
            }
            @Override
            public void onFailure(Throwable caught) {
                Window.alert(caught.getMessage());
            }
        });
		
		postComment.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
		        sessionSvc.getSession(new AsyncCallback<User>() {
		            @Override
		            public void onSuccess(User user) {
		            	if(user != null) {
		            		userSvc.postComment(article, user.getNickname(), contentCommentNew.getText(), new AsyncCallback<Boolean>() {
		            			@Override
		            			public void onFailure(Throwable caught) {
		            				Window.alert(caught.getMessage());
		            			}
		            			@Override
		            			public void onSuccess(Boolean result) {
		            				if(result) {
		            					Window.alert("Commento pubblicato, attendere moderazione!");
		            				}	
		            			}
		            		});
		            	}else {
		            		userSvc.getRegistrationUserPage( new AsyncCallback<RegistrationUserPage>() {
		            			@Override
		            			public void onFailure(Throwable caught) {
		            				Window.alert(caught.getMessage());
		            			}
		            			@Override
		            			public void onSuccess(RegistrationUserPage result) {
		            				RootPanel.get("mainContent").clear();
		            				RootPanel.get("mainContent").add(result.getRegistrationUserPage());
		            			}
		            		});
		            	}
		            }
		            @Override
		            public void onFailure(Throwable caught) {
		                Window.alert(caught.getMessage());
		            }
		        });
            }
		});
		
		getHomePageButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	userSvc.getHomePage( new AsyncCallback<HomePage>() {
        			@Override
        			public void onFailure(Throwable caught) {
        				Window.alert(caught.getMessage());
        			}
        			@Override
        			public void onSuccess(HomePage result) {
        		        sessionSvc.getSession( new AsyncCallback<User>() {

        		            @Override
        		            public void onSuccess(User user) {
        		            	if(user != null) {
        		            		RootPanel.get("mainContent").clear();
        		            		RootPanel.get("mainContent").add(user.getHomePage());
        		            	} else {
        		            		RootPanel.get("mainContent").clear();
        		            		RootPanel.get("mainContent").add(result.getHomePage());
        		            	}
        		            }
        		            @Override
        		            public void onFailure(Throwable caught) {
        		                Window.alert(caught.getMessage());
        		            }
        		        });
        			}
        		});
            }
		});
		
		likeButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	sessionSvc.getSession(new AsyncCallback<User>() {
		            @Override
		            public void onSuccess(User user) {
		            	if(user != null) {
		            		userSvc.likeArticle(user.getNickname(), article, new AsyncCallback<Boolean>() {
		    		            @Override
		    		            public void onSuccess(Boolean result) {
		    		            	if(result) {
		    		            		likeButton.setText("Non mi piace piu'");
		    		            	} else {
		    		            		likeButton.setText("Mi piace");
		    		            	}
		    		            }
		    		            @Override
		    		            public void onFailure(Throwable caught) {
		    		                Window.alert(caught.getMessage());
		    		            }
		    		        });
		            	}else {
		            		userSvc.getRegistrationUserPage( new AsyncCallback<RegistrationUserPage>() {
		            			@Override
		            			public void onFailure(Throwable caught) {
		            				Window.alert(caught.getMessage());
		            			}
		            			@Override
		            			public void onSuccess(RegistrationUserPage result) {
		            				RootPanel.get("mainContent").clear();
		            				RootPanel.get("mainContent").add(result.getRegistrationUserPage());
		            			}
		            		});
		            	}
		            }
		            @Override
		            public void onFailure(Throwable caught) {
		                Window.alert(caught.getMessage());
		            }
		        });
            }
		});
		
		
		////
		sessionSvc.getSession(new AsyncCallback<User>() {
            @Override
            public void onSuccess(User user) {
            	if(user instanceof Admin || user.getNickname().equals(article.getAuthor().getNickname())) {
            		articlePanel.clear();

            		articlePanel.add(getHomePageButton);
            		Label titleArticle = new Label("Titolo dell'articolo* : ");
            		titleArticle.addStyleName("label");
            		articlePanel.add(titleArticle);
            		articlePanel.add(titleInput);
            		Label contentArticle = new Label("Contenuto dell'articolo* : ");
            		contentArticle.addStyleName("label");
            		articlePanel.add(contentArticle);
            		articlePanel.add(contentInput);

            		Label categoryLabel = new Label("Scegli la categoria *:");
            		categoryLabel.addStyleName("label");
            		ListBox categoryList = new ListBox();

            		Label subCategoryLabel = new Label("Scegli la sottocategoria :");
            		subCategoryLabel.addStyleName("label");
            		ListBox subCategoryList = new ListBox();

            		HorizontalPanel buttonsPanel = new HorizontalPanel();
            		Button saveButton = new Button("Applica le modifiche");
            		saveButton.addStyleName("btn");
            		Button removeArticleButton = new Button("Elimina articolo");
            		removeArticleButton.addStyleName("btn");
            		
            		FlexTable table = new FlexTable();
            		idRow = 0;
            		if(user instanceof Admin) {
            			AdminServiceAsync adminSvc = GWT.create(AdminService.class);
	            		for(int i = 0; i < article.getModeratedComments().size(); i++) {
	            			Integer innerId = idRow;

	            			Button removeCommentButton = new Button("Elimina commento");
	            			removeCommentButton.addStyleName("btn");
	            			Label contentComment = new Label(article.getModeratedComments().get(i).getNicknameUser() + ": " + article.getModeratedComments().get(i).getComment());
	            			removeCommentButton.addClickHandler(new ClickHandler() {
	            	            public void onClick(ClickEvent event) {
	            	            	Integer idComment = Integer.valueOf(removeCommentButton.getElement().getId());
	            	            	adminSvc.removeComment(article.getId(), idComment, new AsyncCallback<Boolean>() {
	            	        			@Override
	            	        			public void onFailure(Throwable caught) {
	            	        				Window.alert(caught.getMessage());
	            	        			}
	            	        			@Override
	            	        			public void onSuccess(Boolean result) {
	            	        				if(Window.confirm("Sei sicuro di elimare il commento?")) {
	            	        					table.removeRow(innerId);
	            	        					Window.alert("Commento eliminato con successo");
	            	        				}
	            	        			}
	            	            	});
	            	            }
	            			});
	            			//contentComment.getElement().setId(((Integer) i).toString()+"C");
	            			removeCommentButton.getElement().setId(article.getModeratedComments().get(i).getId().toString());

	            			int row = table.getRowCount();
	            			table.setWidget(row, 0, contentComment);
	            			table.setWidget(row, 1, removeCommentButton);		
	            			
	            			idRow++;
	            		}
            		}
            		
            		userSvc.getCategoriesArrayList(new AsyncCallback<ArrayList<Category>>() {
            			@Override
            			public void onFailure(Throwable caught) {
            				Window.alert("Cannot load symbols: " + caught.getMessage());
            				RootPanel.get("mainContent").add(user.getProfilePage());
            			}
            			@Override
            			public void onSuccess(ArrayList<Category> result) {
            				if(result.size() != 0) {
            					subCategoryList.addItem("Scegli la sottocategoria");
            					for(int i = 0; i < result.size(); i++) {
            						categoryList.addItem(result.get(i).getCategory());
            						if(result.get(i).getCategory().equals(article.getCategory())) {
            							categoryList.setItemSelected(i, true);

            							for(int j = 0; j < result.get(i).getSubCategory().size(); j++) {
            								subCategoryList.addItem(result.get(i).getSubCategory().get(j));
            								if(result.get(i).getSubCategory().get(j).equals(article.getSubCategory())) {
            									subCategoryList.setSelectedIndex(j+1);
            								}
            							}
            						}
            					}

            					categoryList.addChangeHandler(new ChangeHandler() {
                        			@Override
                        			public void onChange(ChangeEvent event) {
                        				subCategoryList.clear();
                        				subCategoryList.addItem("Scegli la sottocategoria");
                        				for(int i = 0; i < result.size(); i++) {
                        					if(result.get(i).getCategory().equals(categoryList.getSelectedItemText())) {
                        						for(int j = 0; j < result.get(i).getSubCategory().size(); j++) {
                        							subCategoryList.addItem(result.get(i).getSubCategory().get(j));
                        						}
                        					}
                        				}
                        			}
                        		});
            				}
            			}
            		});
            		
            		/**/
            		
            		saveButton.addClickHandler(new ClickHandler() {
        	            public void onClick(ClickEvent event) {
        	            	authorSvc.getArticleById(article.getId(), new AsyncCallback<Article>() {
        	        			@Override
        	        			public void onFailure(Throwable caught) {
        	        				Window.alert("Cannot load symbols: " + caught.getMessage());
        	        			}
        	        			@Override
        	        			public void onSuccess(Article articleById) {
        	        				Article a = null;
                	            	if(subCategoryList.getSelectedItemText().equals("Scegli la sottocategoria")) {
                	            		if(articleById instanceof ArticlePremium) {
                	            			a = new ArticlePremium(article.getId(),article.getAuthor(),titleInput.getText(),contentInput.getText(),categoryList.getSelectedItemText(),"",new Date().getTime() / 1000L,article.getLikes(),article.getComments(),article.getModeratedComments(),article.getDeclinedComments());
                	            		}else {
                	            			a = new Article(article.getId(),article.getAuthor(),titleInput.getText(),contentInput.getText(),categoryList.getSelectedItemText(),"",new Date().getTime() / 1000L,article.getLikes(),article.getComments(),article.getModeratedComments(),article.getDeclinedComments());
                	            		}
                	            	} else {
                	            		if(articleById instanceof ArticlePremium) {
                	            			a = new ArticlePremium(article.getId(),article.getAuthor(),titleInput.getText(),contentInput.getText(),categoryList.getSelectedItemText(),subCategoryList.getSelectedItemText(),new Date().getTime() / 1000L,article.getLikes(),article.getComments(),article.getModeratedComments(),article.getDeclinedComments());
                	            		} else {
                	            			a = new Article(article.getId(),article.getAuthor(),titleInput.getText(),contentInput.getText(),categoryList.getSelectedItemText(),subCategoryList.getSelectedItemText(),new Date().getTime() / 1000L,article.getLikes(),article.getComments(),article.getModeratedComments(),article.getDeclinedComments());
                	            		}
                	            	}
                	            	authorSvc.editArticle(a, new AsyncCallback<Boolean>() {
                	        			@Override
                	        			public void onFailure(Throwable caught) {
                	        				Window.alert("Cannot load symbols: " + caught.getMessage());
                	        				RootPanel.get("mainContent").add(user.getProfilePage());
                	        			}
                	        			@Override
                	        			public void onSuccess(Boolean result) {
                	        				if(result) {
                	        					Window.alert("Articolo modificato");
                	        				}
                	        			}
                	        		});	
        	        			}
        	        		});	
        	            }
        			});
        			
        			removeArticleButton.addClickHandler(new ClickHandler() {
        	            public void onClick(ClickEvent event) {
        	            	if(Window.confirm("Sei sicuro di eliminare l'articolo?")) {
        	            		authorSvc.deleteArticle(article.getId(),new AsyncCallback<Boolean>() {
            	        			@Override
            	        			public void onFailure(Throwable caught) {
            	        				Window.alert("Cannot load symbols: " + caught.getMessage());
            	        				RootPanel.get("mainContent").add(user.getProfilePage());
            	        			}
            	        			@Override
            	        			public void onSuccess(Boolean result) {
            	        				if(result) {
            	        					Window.alert("Articolo eliminato");
            	        					RootPanel.get("mainContent").clear();
            	        					RootPanel.get("mainContent").add(user.getHomePage());
            	        				}
            	        			}
            	        		});	
        	            	}
        	            }
        			});

            		articlePanel.add(categoryLabel);
            		articlePanel.add(categoryList);
            		articlePanel.add(subCategoryLabel);
            		subCategoryList.addStyleName("subCategoryList");
            		articlePanel.add(subCategoryList);
            		
            		buttonsPanel.add(saveButton);
            		buttonsPanel.add(removeArticleButton);
            		articlePanel.add(buttonsPanel);
            		if(user instanceof Admin) {
            			HTML html = new HTML("<hr  style=\"width:100%;\" />");
            			articlePanel.add(html);
            			articlePanel.add(table);
            		}
        		}
            }
            @Override
            public void onFailure(Throwable caught) {
                Window.alert(caught.getMessage());
            }
        });
		///
		
		articlePanel.add(getHomePageButton);
		articlePanel.add(titleLabel);
		articlePanel.add(contentLabel);
		articlePanel.add(likeButton);
		for(int i = 0; i < article.getModeratedComments().size(); i++) {
			Label contentComment = new Label(article.getModeratedComments().get(i).getNicknameUser() + ": " + article.getModeratedComments().get(i).getComment()); 
			contentComment.addStyleName("contentComment");
			articlePanel.add(contentComment);
			
		}
		articlePanel.add(contentCommentNew);
		articlePanel.add(postComment);
		
		return articlePanel;
	}
	
	@Override
	public String toString() {
		return title + " " + author + " " + content + " " + category + " " + subCategory;
	}
}
