package bolognino.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface UserServiceAsync {
	
	void getRegistrationUserPage(AsyncCallback<RegistrationUserPage> callback);
	
	void getAccessRegistrationAuthorPage(AsyncCallback<AccessRegistrationAuthorPage> callback);
	
	void getRegistrationAuthorPage(AsyncCallback<RegistrationAuthorPage> callback);
	
	void getHomePage(AsyncCallback<HomePage> callback);
	
	void getLoginPage(AsyncCallback<LoginPage> callback);
	
	void newUser(String nickname, String password, String email, String firstname, String surname, Date birthday, String state, String province, String region, String city, String address, Boolean gender, String birthPlace, ArrayList<String> categories, ArrayList<Article> likesAndComments, AsyncCallback<Boolean> callback);
	
	void newAuthor(String nickname, String password, String email, String firstname, String surname, Date birthday, String state, String province, String region, String city, String address, Boolean gender, String birthPlace, ArrayList<String> categories, ArrayList<Article> likesAndComments, AsyncCallback<Boolean> callback);
	
	void upgradeUser(User user, String ncc, String cardHolderName, String cardHolderSurname, Date cardDeadline, String cvv, String nation, String cap, AsyncCallback<User> callback);
	
	void downgradeUser(User user, AsyncCallback<User> callback);
	
	void getAuthentication(String nickname, String password, AsyncCallback<User> callback);
	
	void checkAuthenticationToken(String token, AsyncCallback<Boolean> callback);
	
	void getCategoriesArray(AsyncCallback<String[]> callback);
	
	void getCategoriesArrayList(AsyncCallback<ArrayList<Category>> callback);
	
	void getArticlesForUser(ArrayList<String> subCategories, AsyncCallback<ArrayList<Article>> callback);
	
	void postComment(Article article, String userNickname, String commentText, AsyncCallback<Boolean> callback);
	
	void likeArticle(String nicknameUser, Article article, AsyncCallback<Boolean> callback);
	
	void getArticlesLikedAndCommentedByUser(String nicknameUser, AsyncCallback<ArrayList<String>> callback);

	void getArticleById(int idArticles, AsyncCallback<Article> callback);
	
	void getUserByNickname(String nicknameUser, AsyncCallback<User> callback);
}
