package bolognino.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Admin extends Subscribed implements Serializable{
	public Admin() {
		
	}
	
	public Admin(String nickname, String password) {
		super(nickname, password,"","","",null,"","","","","",null,"",null,null,"","","",null,"","","");
	}
	
	
	@Override
	public VerticalPanel getHomePage() { //ritorna la pagina principale per un admin
		VerticalPanel mainPanel = new VerticalPanel();
		HorizontalPanel buttonsPanel = new HorizontalPanel();
		Button managePageButton = new Button("Vai alla pagina gestionale");
		managePageButton.addStyleName("btn");
		Button logoutButton = new Button("Logout");
		logoutButton.addStyleName("btn");
		
		managePageButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	RootPanel.get("mainContent").clear();
            	
				RootPanel.get("mainContent").add(getProfilePage());
            }
		});
		
		logoutButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	UserServiceAsync userSvc = GWT.create(UserService.class);
	        	userSvc.getHomePage( new AsyncCallback<HomePage>() {
        			@Override
        			public void onFailure(Throwable caught) {
        				Window.alert(caught.getMessage());
        			}
        			@Override
        			public void onSuccess(HomePage result) {
        				MySessionServiceAsync sessionSvc = GWT.create(MySessionService.class);
        		        sessionSvc.exitSession( new AsyncCallback<Void>() {
        		            @Override
        		            public void onSuccess(Void result2) {
        		            	RootPanel.get("mainContent").clear();
                				RootPanel.get("mainContent").add(result.getHomePage());
        		            }
        		            @Override
        		            public void onFailure(Throwable caught) {
        		                Window.alert(caught.getMessage());
        		            }
        		        });
        			}
        		});
	        }
	    });
		
		buttonsPanel.add(managePageButton);
		buttonsPanel.add(logoutButton);
		
		mainPanel.add(buttonsPanel);
		
		mainPanel.add(getArticles(new ArrayList<String>()));
		
		return mainPanel;
	}
	/*Ritorna la pagina principale per un utente amministratore*/
	@Override
	public VerticalPanel getProfilePage() {
		VerticalPanel mainPanel = new VerticalPanel();
		Button getHomePageButton = new Button("Torna Indietro");
		getHomePageButton.addStyleName("btn");
		Button manageCategory = new Button("Gestisci le categorie");
		manageCategory.addStyleName("btn");
		Button moderateCommentButton = new Button("Moderazione commenti");
		moderateCommentButton.addStyleName("btn");
		Button createArticleButton = new Button("Crea Articolo");
		createArticleButton.addStyleName("btn");

		getHomePageButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
				RootPanel.get("mainContent").clear();
				
				RootPanel.get("mainContent").add(getHomePage());
            }
		});

		manageCategory.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	RootPanel.get("mainContent").clear();
            	
				RootPanel.get("mainContent").add(getManageCategoryPage());
            }
		});

		moderateCommentButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	RootPanel.get("mainContent").clear();
            	
				RootPanel.get("mainContent").add(getModerateCommentPage());
            }
		});
		
		createArticleButton.addClickHandler(new ClickHandler() {
		    public void onClick(ClickEvent event) {
		    	RootPanel.get("mainContent").clear();
            	
				RootPanel.get("mainContent").add(getCreateArticlePage());
	        }
	    });

		mainPanel.add(getHomePageButton);
		mainPanel.add(manageCategory);
		mainPanel.add(moderateCommentButton);
		mainPanel.add(createArticleButton);

		return mainPanel;
	}
	/*Metodo della gestione delle categorie e delle rispettive sottocategorie*/
	public Integer id = 0;
	public ArrayList<String> tableRows = new ArrayList<>();
	public Map<Integer, Row> categoriesMap = new HashMap<Integer, Row>();

	public VerticalPanel getManageCategoryPage() {
		tableRows.clear();
		id = 0;
		
		AdminServiceAsync adminSvc = GWT.create(AdminService.class);
	    VerticalPanel mainPanel = new VerticalPanel();
	    FlexTable table = new FlexTable();
	    HorizontalPanel footerButtons = new HorizontalPanel();
		Button addCategoryButton = new Button("Aggiungi categoria");
		addCategoryButton.addStyleName("btn");
		Button saveButton = new Button("Applica le modifiche");
		saveButton.addStyleName("btn");
		Button getHomePageButton = new Button("Torna indietro");
		getHomePageButton.addStyleName("btn");
		
		getHomePageButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	RootPanel.get("mainContent").clear();
            	
            	RootPanel.get("mainContent").add(getProfilePage());
            }
		});

	    adminSvc.getCategoriesList(new AsyncCallback<ArrayList<Category>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Cannot load symbols: " + caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Category>  result) {
				for(int i = 0 ; i < result.size(); i++) {
					addCategory(table, result.get(i).getCategory(), result.get(i).getSubCategory());
				}
			}
		});

		addCategoryButton.addClickHandler(new ClickHandler() {
		    public void onClick(ClickEvent event) {
		    	boolean anotherVoidCategory = false;
		    	for(Map.Entry<Integer,Row> entry : categoriesMap.entrySet()) {
					if(entry.getValue().equals("")) {
						anotherVoidCategory = true;
					}
				}

		    	if(!anotherVoidCategory) {
			    	addCategory(table, "", new ArrayList<>());
		    	} else {
		    		Window.alert("Hai una categoria senza nome!");
		    	}
	        }
	    });
		
		saveButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	boolean printError = false;
	        	for(Map.Entry<Integer,Row> entry : categoriesMap.entrySet()) {
	        		if(entry.getValue().category.equals("") && !printError) {
	        			Window.alert("C'� una categoria senza nome, non verra' considerata.");
	        			printError = true;
	        		}
				}

	        	Map<String, ArrayList<String>> newCategories = new HashMap<String, ArrayList<String>>();
	        	for(Map.Entry<Integer,Row> entry : categoriesMap.entrySet()) {
	        		if(!entry.getValue().category.equals("")) {
	        			ArrayList<String> subcategories = entry.getValue().subCategory;
	        			if(subcategories.indexOf("") != -1)
	        				subcategories.remove(subcategories.indexOf(""));
	        			newCategories.put(entry.getValue().category, subcategories);
	        		}
				}

	        	adminSvc.replaceCategories(newCategories, new AsyncCallback<Boolean>() {
        			@Override
        			public void onFailure(Throwable caught) {
        				Window.alert("Cannot load symbols: " + caught.getMessage());
        			}
        			@Override
        			public void onSuccess(Boolean result) {
        				if(result) {
        					Window.alert("Categorie aggiornate con successo!");
        					tableRows.clear();
        					id = 0;
        				}
        			}
        		});
	        }
	    });

		Label categoryLabel = new Label("Categoria");
		categoryLabel.addStyleName("label");
	    table.setWidget(0, 0, categoryLabel);
	    Label subCategoryLabel = new Label("Sottocategoria");
	    subCategoryLabel.addStyleName("label");
		table.setWidget(0, 1, subCategoryLabel);
		Label manageSubCategoryLabel = new Label("Gestisci Sottocategorie");
		manageSubCategoryLabel.addStyleName("label");
		table.setWidget(0, 2, manageSubCategoryLabel);
		Label removeCategoryLabel = new Label("Elimina Categoria");
		removeCategoryLabel.addStyleName("label");
		table.setWidget(0, 3, removeCategoryLabel);

		footerButtons.add(addCategoryButton);
		footerButtons.add(saveButton);

		mainPanel.add(getHomePageButton);
		mainPanel.add(table);
		mainPanel.add(footerButtons);

		return mainPanel;
	}
	
	public void addCategory(FlexTable table, String category, ArrayList<String> subcategories) {
		Integer innerId = id;
		tableRows.add(innerId.toString());

		TextBox categoryTextBox = new TextBox();
		VerticalPanel subCategory = new VerticalPanel();
		ListBox subCategoryListBox = new ListBox();
		TextBox selectedSubcategoryTextBox = new TextBox();
		HorizontalPanel editSubcategory = new HorizontalPanel();
		Button addSubcategoryButton = new Button("+");
		addSubcategoryButton.addStyleName("btn");
		Button removeSubcategoryButton = new Button("x");
		removeSubcategoryButton.addStyleName("btn");
		Button removeCategoryButton = new Button("x");
		removeCategoryButton.addStyleName("btn");
		
		categoriesMap.put(innerId, new Row(category, subcategories));

    	categoryTextBox.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				boolean anotherOneEqual = false; //categorie uguali
				ArrayList<String> allCategories = new ArrayList<String>(); 

				for(Map.Entry<Integer,Row> entry : categoriesMap.entrySet()) {
					allCategories.add(entry.getValue().category);
				}

				if(allCategories.indexOf("") != -1)
					allCategories.remove((allCategories.indexOf("")));

				if(allCategories.indexOf(categoryTextBox.getText()) != -1) {	//se ci sono altre categorie uguali
					anotherOneEqual = true;
				}

				//Controlliamo che non ci sia un'altra categoria uguale o che la textBox non sia vuota
				if(categoryTextBox.getText().equals("") || anotherOneEqual) {
					categoryTextBox.setText(categoriesMap.get(innerId).category);
				} else { //Se l'inserimento pu� essere fatto
					categoriesMap.put(innerId, new Row(categoryTextBox.getText(), categoriesMap.get(innerId).subCategory));
				}
			}
		});

		selectedSubcategoryTextBox.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				if(!subCategoryListBox.getSelectedValue().equals("Scegli la sottocategoria") && !selectedSubcategoryTextBox.getText().equals("Scegli la sottocategoria")) {
					ArrayList<String> allSubcategories = new ArrayList<String>(); 
					
					for(Map.Entry<Integer,Row> entry : categoriesMap.entrySet()) {
						for(int i = 0; i < entry.getValue().subCategory.size(); i++) {
							allSubcategories.add(entry.getValue().subCategory.get(i));
						}
					}

					allSubcategories.remove((allSubcategories.indexOf("")));

					if(allSubcategories.indexOf(selectedSubcategoryTextBox.getText()) == -1) {	//se non altre categorie uguali
						subCategoryListBox.setItemText(subCategoryListBox.getSelectedIndex(), selectedSubcategoryTextBox.getText());
						ArrayList<String> subcategories = categoriesMap.get(innerId).subCategory;
						subcategories.set(subcategories.indexOf(""), selectedSubcategoryTextBox.getText());
						categoriesMap.put(innerId, new Row(categoryTextBox.getText(), subcategories));
					} else {
						Window.alert("Questa sottocategoria risulta gi� presente fra le sottocategorie.");
						selectedSubcategoryTextBox.setText("");
					}
				} else if(selectedSubcategoryTextBox.getText().equals("Scegli la sottocategoria")) {
					Window.alert("Non puoi inserire questa categoria!");
					selectedSubcategoryTextBox.setText("");
				}
			}
		});

		subCategoryListBox.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				int index = subCategoryListBox.getSelectedIndex(); 

				if(!subCategoryListBox.getItemText(index).equals("Scegli la sottocategoria"))
					selectedSubcategoryTextBox.setText(subCategoryListBox.getItemText(index));
				else
					selectedSubcategoryTextBox.setText("");
			}
		});

		addSubcategoryButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	if(categoriesMap.get(innerId).subCategory.indexOf("") == -1) {	//Se non esiste una sottocategoria vuota
	        		ArrayList<String> subcategories = categoriesMap.get(innerId).subCategory;
	        		subcategories.add("");
	        		selectedSubcategoryTextBox.setText("");
		        	subCategoryListBox.addItem("");
		        	subCategoryListBox.setItemSelected(subCategoryListBox.getItemCount()-1, true);
	        	}
	        }
	    });

		removeSubcategoryButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	if(subCategoryListBox.getSelectedItemText() != "Scegli la sottocategoria") {
	        		subCategoryListBox.removeItem(subCategoryListBox.getSelectedIndex());
	        		selectedSubcategoryTextBox.setText("");

	        		ArrayList<String> subcategories = categoriesMap.get(innerId).subCategory;
	        		subcategories.remove(subcategories.indexOf(selectedSubcategoryTextBox.getText()));
	        		categoriesMap.put(innerId, new Row(categoryTextBox.getText(), subcategories));
	        	}
	        }
	    });

		removeCategoryButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	int removedIndex = tableRows.indexOf(removeCategoryButton.getElement().getId());
	        	tableRows.remove(removedIndex);
	            table.removeRow(removedIndex + 1);

	            categoriesMap.remove(innerId);
	        }
	    });

		//Lato front-end
		categoryTextBox.setText(category);
		categoryTextBox.getElement().setId((innerId).toString());

    	subCategoryListBox.addItem("Scegli la sottocategoria");
    	
    	for(int i = 0; i < subcategories.size(); i++) {
			subCategoryListBox.addItem(subcategories.get(i));
		}

		subCategory.add(subCategoryListBox);
		subCategory.add(selectedSubcategoryTextBox);

		editSubcategory.add(addSubcategoryButton);
		editSubcategory.add(removeSubcategoryButton);

		int row = table.getRowCount();
		removeCategoryButton.getElement().setId(((Integer) (innerId)).toString()); //Quando elimino la riga devo sapere che riga eliminare dalla tabella
		table.setWidget(row, 0, categoryTextBox);			//name category
		table.setWidget(row, 1, subCategory);				// list subcategory
		table.setWidget(row, 2, editSubcategory);			// button manage subcategory
		table.setWidget(row, 3, removeCategoryButton);		// remove category

		id++;
	}
	/*Metodo che restituisce la pagina per la moderazione dei commenti*/
	public VerticalPanel getModerateCommentPage() {
		tableRows.clear();
		id = 0;

		RootPanel.get("mainContent").clear();
		VerticalPanel moderatePanel = new VerticalPanel();
		AdminServiceAsync adminSvc = GWT.create(AdminService.class);
		
		Button getHomePageButton = new Button("Torna indietro");
		getHomePageButton.addStyleName("btn");
		getHomePageButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	RootPanel.get("mainContent").clear();
            	
            	RootPanel.get("mainContent").add(getProfilePage());
            }
		});
		
		adminSvc.getArticlesWithCommentsToModerate(new AsyncCallback<ArrayList<Article>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Article> result) {
				FlexTable table = new FlexTable();
				Label authorLabel = new Label("Autore");
				authorLabel.addStyleName("label");
				table.setWidget(0, 0, authorLabel);
				Label articleLabel = new Label("Articolo");
				articleLabel.addStyleName("label");
				table.setWidget(0, 1, articleLabel);
				Label userLabel = new Label("Utente");
				userLabel.addStyleName("label");
				table.setWidget(0, 2, userLabel);
				Label commentLabel = new Label("Commento");
				commentLabel.addStyleName("label");
				table.setWidget(0, 3, commentLabel);
				Label approvaLabel = new Label("Approva");
				approvaLabel.addStyleName("label");
				table.setWidget(0, 4, approvaLabel);
				Label disapprovaLabel = new Label("Disapprova");
				disapprovaLabel.addStyleName("label");
				table.setWidget(0, 5, disapprovaLabel);

				for(int i = 0; i < result.size(); i++) {
					for(int j = 0; j < result.get(i).getComments().size(); j++) {
						int countRow = table.getRowCount();
						Integer innerId = id;
						tableRows.add(innerId.toString());
						int articleId = result.get(i).getId();
						int commentId = result.get(i).getComments().get(j).getId();
						Button approveCommentButton = new Button("OK");
						approveCommentButton.addStyleName("btn");
						Button declineCommentButton = new Button("X");
						declineCommentButton.addStyleName("btn");
						
						approveCommentButton.addClickHandler(new ClickHandler() {
				            public void onClick(ClickEvent event) {
				            	int removedIndex = tableRows.indexOf(approveCommentButton.getElement().getId().substring(0,approveCommentButton.getElement().getId().length()-1));
					        	tableRows.remove(removedIndex);
					            table.removeRow(removedIndex + 1);
				            	
				            	adminSvc.approveComment(articleId, commentId, new AsyncCallback<Boolean>() {
				        			@Override
				        			public void onFailure(Throwable caught) {
				        				Window.alert("Cannot load symbols: " + caught.getMessage());
				        			}
				        			@Override
				        			public void onSuccess(Boolean result) {
				        				if(result){
				        					Window.alert("Commento moderato con successo!");
				        				}
				        			}
				        		});
				            }
						});
						
						approveCommentButton.getElement().setId(((Integer) (innerId)).toString()+ "A");
						
						declineCommentButton.addClickHandler(new ClickHandler() {
				            public void onClick(ClickEvent event) {
				            	int removedIndex = tableRows.indexOf(approveCommentButton.getElement().getId().substring(0,approveCommentButton.getElement().getId().length()-1));
					        	tableRows.remove(removedIndex);
					            table.removeRow(removedIndex + 1);
					            
					            adminSvc.declineComment(articleId, commentId, new AsyncCallback<Boolean>() {
				        			@Override
				        			public void onFailure(Throwable caught) {
				        				Window.alert("Cannot load symbols: " + caught.getMessage());
				        			}
				        			@Override
				        			public void onSuccess(Boolean result) {
				        				if(result){
				        					Window.alert("Commento archiviato con successo!");
				        				}
				        			}
				        		});
					            
					            
				            }
						});
						
						declineCommentButton.getElement().setId(((Integer) (innerId)).toString()+ "D");
						
						table.setWidget(countRow, 0, new Label(result.get(i).getAuthor().getNickname()));
						table.setWidget(countRow, 1, new Label(result.get(i).getTitle()));
						table.setWidget(countRow, 2, new Label(result.get(i).getComments().get(j).getNicknameUser()));
						table.setWidget(countRow, 3, new Label(result.get(i).getComments().get(j).getComment()));
						table.setWidget(countRow, 4, approveCommentButton);
						table.setWidget(countRow, 5, declineCommentButton);
						
						id++;
					}
					table.addStyleName("table");
					moderatePanel.add(table);
				}
			}
		});
		
		
		moderatePanel.add(getHomePageButton);
		Label titleLabel = new Label("Elenco di articoli con commenti da moderare:");
		titleLabel.addStyleName("label");
		moderatePanel.add(titleLabel);
		return moderatePanel;
	}
	/*Metodo che restituisce la pagina di creazione di un articolo*/
	public VerticalPanel getCreateArticlePage() {
		VerticalPanel articlePanel = new VerticalPanel();
		VerticalPanel creationArticlePanel = new VerticalPanel();
		AdminServiceAsync adminSvc = GWT.create(AdminService.class);
		
		Button getHomePageButton = new Button("Torna indietro");
		getHomePageButton.addStyleName("btn");
		ListBox authorListBox = new ListBox();
		authorListBox.addItem("Seleziona autore");
		ArrayList<User> authorsList = new ArrayList<User>();
		
		getHomePageButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	RootPanel.get("mainContent").clear();
            	
            	RootPanel.get("mainContent").add(getProfilePage());
            }
		});
		
		adminSvc.getAuthorRegistred(new AsyncCallback<ArrayList<User>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<User> authors) {
				for(int i = 0; i < authors.size(); i++) {
					authorsList.add(authors.get(i));
					authorListBox.addItem(authors.get(i).getNickname());
				}
			}
		});
		
		authorListBox.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				creationArticlePanel.clear();
				int index = authorListBox.getSelectedIndex()-1;
				Author author = new Author(authorsList.get(index).getNickname(), authorsList.get(index).getPassword(), authorsList.get(index).getEmail(), authorsList.get(index).getFirstname(), authorsList.get(index).getSurname(), authorsList.get(index).getBirthday(), authorsList.get(index).getState(), authorsList.get(index).getProvince(), authorsList.get(index).getRegion(), authorsList.get(index).getCity(),authorsList.get(index).getAddress(), authorsList.get(index).getGender(), authorsList.get(index).getBirthPlace(), authorsList.get(index).getCategories(), authorsList.get(index).getLikesAndComments());
				creationArticlePanel.add(getCreationAritclePage(author));
				articlePanel.add(creationArticlePanel);
			}
		});
		
		articlePanel.add(getHomePageButton);
		articlePanel.add(authorListBox);
		
		return articlePanel;
	}
	
	private ArrayList<Category> categories = new ArrayList<Category>();
	public VerticalPanel getCreationAritclePage(Author author){
		AuthorServiceAsync authorSvc = GWT.create(AuthorService.class);
		AdminServiceAsync adminSvc = GWT.create(AdminService.class);
		VerticalPanel createArticlePanel = new VerticalPanel();
		
    	Label titleArticleLabel = new Label("Titolo dell'articolo *:");
    	titleArticleLabel.addStyleName("label");
    	TextBox titleArticleInput = new TextBox();
    	titleArticleInput.addStyleName("titleArticleTextArea");
    	
    	Label contentArticleLabel = new Label("Contenuto *:");
    	contentArticleLabel.addStyleName("label");
    	TextArea contentArticleInput = new TextArea();
    	contentArticleInput.addStyleName("contentArticleTextArea");
    	
    	RadioButton articleTypeFree = new RadioButton("myRadioGroup", "Free");
		RadioButton articleTypePremium = new RadioButton("myRadioGroup", "Premium");
		
		Label categoryLabel = new Label("Scegli la categoria *:");
		categoryLabel.addStyleName("label");
		ListBox categoryList = new ListBox();
		categoryList.addItem("Scegli la categoria");
		
		Label subCategoryLabel = new Label("Scegli la sottocategoria :");
		subCategoryLabel.addStyleName("label");
		ListBox subCategoryList = new ListBox();

		adminSvc.getCategoriesList(new AsyncCallback<ArrayList<Category>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Cannot load symbols: " + caught.getMessage());
				RootPanel.get("mainContent").add(getProfilePage());
			}
			@Override
			public void onSuccess(ArrayList<Category> result) {
				categories = result;
				if(result.size() != 0) {
					for(int i = 0; i < result.size(); i++) {
						categoryList.addItem(result.get(i).getCategory());
					}
				}
			}
		});	
		
		categoryList.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				subCategoryList.clear();
				subCategoryList.addItem("Scegli la sottocategoria");
				for(int i = 0; i < categories.size(); i++) {
					if(categories.get(i).getCategory().equals(categoryList.getSelectedItemText())) {
						for(int j = 0; j < categories.get(i).getSubCategory().size(); j++) {
							subCategoryList.addItem(categories.get(i).getSubCategory().get(j));
						}
					}
				}
			}
		});
		
		Button confirmCreationButton = new Button("Crea articolo");
		confirmCreationButton.addStyleName("btn");

		confirmCreationButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	if((articleTypeFree.getValue() || articleTypePremium.getValue()) && titleArticleInput.getText().length() > 0 && contentArticleInput.getText().length() > 0 && categoryList.getSelectedItemText().length() > 0 && !categoryList.getSelectedItemText().equals("Scegli la categoria")) {
		        	if(Window.confirm("Vuoi procedere con la creazione dell'articolo?")) {
		            	if(articleTypeFree.getValue()) {
		            		String subCategory = "";
		            		
		            		if(subCategoryList.getSelectedItemText() != null) {
			            		if(!subCategoryList.getSelectedItemText().equals("Scegli la sottocategoria")) {
			            			subCategory = subCategoryList.getSelectedItemText();
			            		}
		            		}
		            		authorSvc.newArticle(author, titleArticleInput.getText(), contentArticleInput.getText(), categoryList.getSelectedItemText(), subCategory, new AsyncCallback<Boolean>() {
			        			@Override
			        			public void onFailure(Throwable caught) {
			        				Window.alert("Cannot load symbols: " + caught.getMessage());
			        				RootPanel.get("mainContent").add(getProfilePage());
			        			}
			        			@Override
			        			public void onSuccess(Boolean result) {
			        				RootPanel.get("mainContent").clear();
			        				if(result) {
			        					Window.alert("Articolo creato con successo!");
			        					RootPanel.get("mainContent").add(getProfilePage());
			        				} else {
			        					Window.alert("La creazione dell'articolo non � andata a buon fine. Riprovare");
			        					RootPanel.get("mainContent").add(getProfilePage());
			        				}
			        			}
			        		});	
		            	} else {
		            		String subCategory = "";
		            		
		            		if(subCategoryList.getSelectedItemText() != null) {
			            		if(!subCategoryList.getSelectedItemText().equals("Scegli la sottocategoria")) {
			            			subCategory = subCategoryList.getSelectedItemText();
			            		}
		            		}
		            		authorSvc.newArticlePremium(author, titleArticleInput.getText(), contentArticleInput.getText(), categoryList.getSelectedItemText(), subCategory, new AsyncCallback<Boolean>() {
			        			@Override
			        			public void onFailure(Throwable caught) {
			        				Window.alert("Cannot load symbols: " + caught.getMessage());
			        				RootPanel.get("mainContent").add(getProfilePage());
			        			}
			        			@Override
			        			public void onSuccess(Boolean result) {
			        				RootPanel.get("mainContent").clear();
			        				if(result) {
			        					Window.alert("Articolo creato con successo!");
			        					RootPanel.get("mainContent").add(getProfilePage());
			        				} else {
			        					Window.alert("La creazione dell'articolo non � andata a buon fine. Riprovare");
			        					RootPanel.get("mainContent").add(getProfilePage());
			        				}
			        			}
			        		});	
		            	}
		        	}
	        	} else {
	        		Window.alert("Compila tutti i campi obbligatori!");
	        	}
	        }
	    });
		createArticlePanel.add(titleArticleLabel);
		createArticlePanel.add(titleArticleInput);
		createArticlePanel.add(contentArticleLabel);
		createArticlePanel.add(contentArticleInput);
		createArticlePanel.add(articleTypeFree);
		createArticlePanel.add(articleTypePremium);
		createArticlePanel.add(categoryLabel);
		createArticlePanel.add(categoryList);
		createArticlePanel.add(subCategoryLabel);
		createArticlePanel.add(subCategoryList);
		createArticlePanel.add(confirmCreationButton);
		
		return createArticlePanel;
	}
}

class Row {
	public String category;
	public ArrayList<String> subCategory;
	
	public Row(String category, ArrayList<String> subCategory) {
		this.category = category;
		this.subCategory = subCategory;
	}
}