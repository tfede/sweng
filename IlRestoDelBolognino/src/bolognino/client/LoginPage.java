package bolognino.client;

import java.io.Serializable;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class LoginPage implements Serializable {

	public LoginPage() {
		
	}
	
	public VerticalPanel getLoginPage() {
		UserServiceAsync userSvc = GWT.create(UserService.class);
		MySessionServiceAsync sessionSvc = GWT.create(MySessionService.class); 
		
		VerticalPanel mainPanel = new VerticalPanel();
		Button getHomePageButton = new Button("Torna alla home");
		getHomePageButton.addStyleName("btn");
		
		Label nicknameLabel = new Label("Nickname:");
		nicknameLabel.addStyleName("label");
		TextBox nicknameInput = new TextBox();
		
		//nicknameInput.getElement().setAttribute("required", "required");		non serve ad un cazzo
		
		Label passwordLabel = new Label("Password:");
		passwordLabel.addStyleName("label");
		PasswordTextBox passwordInput = new PasswordTextBox();
		
		Label error = new Label("Credenziali non corrette");
		
		Button loginButton = new Button("Login");
		loginButton.addStyleName("btn");
		
		getHomePageButton.addStyleName("pure-button");
		
		mainPanel.add(getHomePageButton);
		mainPanel.add(nicknameLabel);
		mainPanel.add(nicknameInput);
		mainPanel.add(passwordLabel);
		mainPanel.add(passwordInput);
		mainPanel.add(loginButton);
		
		loginButton.addStyleName("pure-button");
		
		
		getHomePageButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	userSvc.getHomePage( new AsyncCallback<HomePage>() {
        			@Override
        			public void onFailure(Throwable caught) {
        				//Window.alert("Cannot load symbols: " + caught.getMessage());
        			}
        			@Override
        			public void onSuccess(HomePage result) {
        				MySessionServiceAsync sessionSvc = GWT.create(MySessionService.class);
        				
        		        sessionSvc.getSession( new AsyncCallback<User>() {

        		            @Override
        		            public void onSuccess(User user) {
        		                // here is the result
        		            	if(user != null) {
        		            		RootPanel.get("mainContent").clear();
        		            		RootPanel.get("mainContent").add(user.getHomePage());
        		            	} else {
        		            		RootPanel.get("mainContent").clear();
        	        				RootPanel.get("mainContent").add(result.getHomePage());
        		            	}
        		            }
        		            @Override
        		            public void onFailure(Throwable caught) {
        		                Window.alert(caught.getMessage());
        		            }
        		        });
        			}
        		});
            }
		});
		
		
		loginButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	String nickname = nicknameInput.getText();
    			String password = passwordInput.getText();
            	userSvc.getAuthentication(nickname, password, new AsyncCallback<User>() {
        			@Override
        			public void onFailure(Throwable caught) {
        				//Window.alert("Cannot load symbols: " + caught.getMessage());
        			}
        			@Override
        			public void onSuccess(User user) {
        				
        				//RootPanel.get("mainContent").clear();
        				if(user == null) {
        					RootPanel.get("mainContent").add(error);
        				}
        				else {		//LOGGATO CON SUCCESSO!!
        					sessionSvc.setSession(user, new AsyncCallback<Void>() {
        			            @Override
        			            public void onSuccess(Void result) {
        			                // here is the result
        			            	RootPanel.get("mainContent").clear();
        			            	RootPanel.get("mainContent").add(user.getHomePage());
        			            }

        			            @Override
        			            public void onFailure(Throwable caught) {
        			                Window.alert(caught.getMessage());

        			            }
        			        });
        					//RootPanel.get("mainContent").add(error);
        				}
        				//RootPanel.get("mainContent").add(result);
        			}
        		});
            }
		});
		return mainPanel;	
	}
	
	
}
