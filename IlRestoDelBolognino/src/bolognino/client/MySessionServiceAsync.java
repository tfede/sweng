package bolognino.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface MySessionServiceAsync {

    void setSession(User user, AsyncCallback<Void> callback);

    void exitSession(AsyncCallback<Void> callback);
    
    void getSession(AsyncCallback<User> callback);
}
