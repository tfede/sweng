package bolognino.client;

import java.util.ArrayList;
import java.util.Map;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface AdminServiceAsync {
	
	void replaceCategories(Map<String,ArrayList<String>> newCategories, AsyncCallback<Boolean> callback);
	
	void getCategoriesList(AsyncCallback<ArrayList<Category>> callback);
	
	void getArticlesWithCommentsToModerate(AsyncCallback<ArrayList<Article>> callback);
	
	void getAllArticles(AsyncCallback<ArrayList<Article>> callback);
	
	void approveComment(int idArticle, int idComment, AsyncCallback<Boolean > callback);
	
	void declineComment(int idArticle, int idComment, AsyncCallback<Boolean > callback);
	
	void getAuthorRegistred(AsyncCallback<ArrayList<User>> callback);
	
	void replaceArticles(Map<Integer, Article> articles, AsyncCallback<Boolean> callback);
	
	void removeComment(int idArticle, int idComment, AsyncCallback<Boolean> callback);
}
