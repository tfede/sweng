package bolognino.client;

import java.util.ArrayList;
import java.util.Map;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("admin")
public interface AdminService extends RemoteService{
	
	Boolean replaceCategories(Map<String,ArrayList<String>> newCategories);
	
	ArrayList<Category> getCategoriesList();
	
	ArrayList<Article> getArticlesWithCommentsToModerate();
	
	ArrayList<Article> getAllArticles();
	
	Boolean approveComment(int idArticle, int idComment);
	
	Boolean declineComment(int idArticle, int idComment);
	
	ArrayList<User> getAuthorRegistred();
	
	Boolean replaceArticles(Map<Integer, Article> articles);
	
	Boolean removeComment(int idArticle, int idComment);
}
