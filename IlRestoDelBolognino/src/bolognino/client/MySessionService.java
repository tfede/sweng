package bolognino.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("session")
public interface MySessionService extends RemoteService {

    public void setSession(User user);

    public void exitSession();
    
    public User getSession();
}
