package bolognino.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DatePicker;

public class User implements Serializable {
	private String nickname;
	private String password;
	private String email;
	private String firstname;
	private String surname;
	private Date birthday;
	private String state;
	private String province;
	private String region;
	private String city;
	private String address;
	private Boolean gender; //true -> F (emale) / false -> M (ale) / null -> X
	private String birthPlace;
	private ArrayList<String> categories;
	private ArrayList<String> likesAndComments;		//    Id-Ha aggiunto il commento all'articolo NOME DELL'ARTICOLO
	
	public User() {
		
	}
	
	public User(String nickname, String password, String email, String firstname, String surname, Date birthday, String state, String province, String region, String city, String address, Boolean gender, String birthPlace, ArrayList<String> categories, ArrayList<String> likesAndComments) {
		this.nickname = nickname;
		this.password = password;
		this.email = email;
		this.firstname = firstname;
		this.surname = surname;
		this.birthday = birthday;
		this.state = state;
		this.province = province;
		this.region = region;
		this.city = city;
		this.address = address;
		this.gender = gender;
		this.birthPlace = birthPlace;
		this.categories = categories;
		this.likesAndComments = likesAndComments;
	}

	public String getNickname() {
		return nickname;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getSurname() {
		return surname;
	}

	public Date getBirthday() {
		return birthday;
	}

	public String getState() {
		return state;
	}

	public String getProvince() {
		return province;
	}

	public String getRegion() {
		return region;
	}

	public String getCity() {
		return city;
	}

	public String getAddress() {
		return address;
	}

	public Boolean getGender() {
		return gender;
	}
	
	public String getBirthPlace() {
		return birthPlace;
	}
		
	public ArrayList<String> getCategories(){
		return categories;
	}

	public ArrayList<String> getLikesAndComments(){
		return likesAndComments;
	}
	
	//articoli free con possobilit� di commento... + getMainPage()
	public VerticalPanel getHomePage() { //ritorna la pagina principale per un utente iscritto non abbonato
		VerticalPanel mainPanel = new VerticalPanel();
		VerticalPanel articlePanel = new VerticalPanel();

		HorizontalPanel buttonsPanel = new HorizontalPanel();
		Button showYourInfo = new Button("Visualizza il tuo profilo");
		showYourInfo.addStyleName("btn");
		Button logout = new Button("Logout");
		logout.addStyleName("btn");
		
		HorizontalPanel articleButtonsPanel = new HorizontalPanel();
		RadioButton favouriteArticlesRButton = new RadioButton("myRadioGroup", "Gli articoli per te");
		favouriteArticlesRButton.setChecked(true);
		RadioButton allArticlesRButton = new RadioButton("myRadioGroup", "Ultime News");
		
		showYourInfo.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	RootPanel.get("mainContent").clear();
	        	
	        	RootPanel.get("mainContent").add(getProfilePage());
	        }
	    });
		
		logout.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	UserServiceAsync userSvc = GWT.create(UserService.class);
	        	userSvc.getHomePage( new AsyncCallback<HomePage>() {
        			@Override
        			public void onFailure(Throwable caught) {
        				Window.alert(caught.getMessage());
        			}
        			@Override
        			public void onSuccess(HomePage result) {
        				MySessionServiceAsync sessionSvc = GWT.create(MySessionService.class);
        		        sessionSvc.exitSession( new AsyncCallback<Void>() {
        		            @Override
        		            public void onSuccess(Void result2) {
        		            	RootPanel.get("mainContent").clear();
                				RootPanel.get("mainContent").add(result.getHomePage());
        		            }
        		            @Override
        		            public void onFailure(Throwable caught) {
        		                Window.alert(caught.getMessage());
        		            }
        		        });
        			}
        		});
	        }
	    });
		
		favouriteArticlesRButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	articlePanel.clear();
	        	articlePanel.add(getArticles(getCategories()));
	        	RootPanel.get("mainContent").add(articlePanel);
	        }
	    });
		
		allArticlesRButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	articlePanel.clear();
	        	articlePanel.add(getArticles(new ArrayList<String>()));
	        	RootPanel.get("mainContent").add(articlePanel);
	        }
	    });
		
		Label welcomeLabel = new Label("Bentornato: " + getNickname());
		welcomeLabel.addStyleName("welcome");
		mainPanel.add(welcomeLabel);
		
		buttonsPanel.add(showYourInfo);
		buttonsPanel.add(logout);
		mainPanel.add(buttonsPanel);
		
		articleButtonsPanel.add(favouriteArticlesRButton);
		articleButtonsPanel.add(allArticlesRButton);
		
		mainPanel.add(articleButtonsPanel);
		
		articlePanel.add(getArticles(getCategories()));
		
		mainPanel.add(articlePanel);
		
		return mainPanel;
	}

	//getInformationPage() + getPaymentPage()
	public VerticalPanel getProfilePage() { //ritorna la pagina personale per un utente iscritto non abbonato
		VerticalPanel mainPanel = new VerticalPanel();
		
		HorizontalPanel buttonsPanel = new HorizontalPanel();
		Button buttonHomePage = new Button("Torna indietro");
		buttonHomePage.addStyleName("btn");
		Button showYourInfo = new Button("Visualizza dati inseriti");
		showYourInfo.addStyleName("btn");
		Button subscribedButton = new Button("Abbonati al giornale");
		subscribedButton.addStyleName("btn");
		
		buttonHomePage.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	RootPanel.get("mainContent").clear();
            	RootPanel.get("mainContent").add(getHomePage());
            }
		});
		
		showYourInfo.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	RootPanel.get("mainContent").clear();
	        	RootPanel.get("mainContent").add(getUserInformationPage());
	        }
	    });

		subscribedButton.addClickHandler(new ClickHandler() {
	        public void onClick(ClickEvent event) {
	        	RootPanel.get("mainContent").clear();
	        	
	        	RootPanel.get("mainContent").add(getPaymentPage());
	        }
	    });
		
		Label welcomeLabel = new Label("Bentornato: " + getNickname());
		welcomeLabel.addStyleName("welcome");
		mainPanel.add(welcomeLabel);
		
		buttonsPanel.add(buttonHomePage);
		buttonsPanel.add(showYourInfo);
		buttonsPanel.add(subscribedButton);
		mainPanel.add(buttonsPanel);
		
		mainPanel.add(getYourLikesAndComments(getNickname()));
		
		return mainPanel;
	}

	public String getLoginPage() {
		return "Login";
	}

	public String getRegistrationPage() {
		return "Registration";
	}
	
	public VerticalPanel getUserInformationPage() {
		VerticalPanel yourInfoPanel = new VerticalPanel();
		Button buttonHomePage = new Button("Torna al profilo");
		buttonHomePage.addStyleName("btn");
		
		buttonHomePage.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	RootPanel.get("mainContent").clear();
            	RootPanel.get("mainContent").add(getProfilePage());
            }
		});
		yourInfoPanel.add(buttonHomePage);
		Label nicknameLabel = new Label("Nickname:");
		nicknameLabel.addStyleName("label");
		yourInfoPanel.add(nicknameLabel);
    	yourInfoPanel.add(new Label(getNickname()));
    	Label emailLabel = new Label("Email:");
    	emailLabel.addStyleName("label");
    	yourInfoPanel.add(emailLabel);
    	yourInfoPanel.add(new Label(getEmail()));
    	Label nameLabel = new Label("Nome:");
    	nameLabel.addStyleName("label");
    	yourInfoPanel.add(nameLabel);
    	yourInfoPanel.add(new Label(getFirstname()));
    	Label surnameLabel = new Label("Cognome:");
    	surnameLabel.addStyleName("label");
    	yourInfoPanel.add(surnameLabel);
    	yourInfoPanel.add(new Label(getSurname()));
    	Label birthdayLabel = new Label("Compleanno:");
    	birthdayLabel.addStyleName("label");
    	yourInfoPanel.add(birthdayLabel);
    	
    	DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
		yourInfoPanel.add(new Label(dateFormat.format(getBirthday())));
    	
    	Label stateLabel = new Label("Stato:");
    	stateLabel.addStyleName("label");
    	yourInfoPanel.add(stateLabel);
    	yourInfoPanel.add(new Label(getState()));
    	Label region = new Label("Regione:");
    	region.addStyleName("label");
    	yourInfoPanel.add(region);
    	yourInfoPanel.add(new Label(getRegion()));
    	Label provinceLabel = new Label("Provincia:");
    	provinceLabel.addStyleName("label");
    	yourInfoPanel.add(provinceLabel);
    	yourInfoPanel.add(new Label(getProvince()));
    	Label cityLabel = new Label("Citta':");
    	cityLabel.addStyleName("label");
    	yourInfoPanel.add(cityLabel);
    	yourInfoPanel.add(new Label(getCity()));
    	Label addressLabel = new Label("Indirizzo:");
    	addressLabel.addStyleName("label");
    	yourInfoPanel.add(addressLabel);
    	yourInfoPanel.add(new Label(getAddress()));
    	
    	
    	if(getGender() != null) {
    		Label genderLabel = new Label("Genere:");
    		genderLabel.addStyleName("label");
    		yourInfoPanel.add(genderLabel);
    		if(getGender()) {
    			yourInfoPanel.add(new Label("Femmina"));
    		}else {
    			yourInfoPanel.add(new Label("Maschio"));
    		}
    	}
    	if(getBirthPlace().length() != 0) {
    		Label birthPlaceLabel = new Label("Luogo di nascita:");
    		birthPlaceLabel.addStyleName("label");
    		yourInfoPanel.add(birthPlaceLabel);
    		yourInfoPanel.add(new Label(getBirthPlace()));
    	}
    	if(getCategories().size() != 0) {
    		Label favouriteCategories = new Label("Categorie preferite:");
    		favouriteCategories.addStyleName("label");
    		yourInfoPanel.add(favouriteCategories);
    		String categoriesFavourite = "";
        	for(int i = 0; i < getCategories().size(); i++) {
        		categoriesFavourite += getCategories().get(i) + " "; 
        	}
        	yourInfoPanel.add(new Label(categoriesFavourite));
    	}
    	return yourInfoPanel;
	}
	
	public VerticalPanel getPaymentPage() {
		VerticalPanel paymentPanel = new VerticalPanel();
		Button buttonHomePage = new Button("Torna al profilo");
		buttonHomePage.addStyleName("btn");
		
		buttonHomePage.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	RootPanel.get("mainContent").clear();
            	RootPanel.get("mainContent").add(getProfilePage());
            }
		});
		paymentPanel.add(buttonHomePage);
		Label nccLabel = new Label("Numero carta *: ");
		nccLabel.addStyleName("label");
		paymentPanel.add(nccLabel);
		TextBox nccInput = new TextBox();
		paymentPanel.add(nccInput);
		Label nameHolderLabel = new Label("Nome titolare *:");
		nameHolderLabel.addStyleName("label");
		paymentPanel.add(nameHolderLabel);
		TextBox cardHolderNameInput = new TextBox();
		paymentPanel.add(cardHolderNameInput);
		Label surnameHolder = new Label("Cognome titolare *:");
		surnameHolder.addStyleName("label");
		paymentPanel.add(surnameHolder);
		TextBox cardHolderSurnameInput = new TextBox();
		paymentPanel.add(cardHolderSurnameInput);
		Label deadlineDateLabel = new Label("Data scadenza *:");
		deadlineDateLabel.addStyleName("label");
		paymentPanel.add(deadlineDateLabel);
		DatePicker cardDeadlineInput = new DatePicker();
		paymentPanel.add(cardDeadlineInput);
		Label cvvLabel = new Label("CVV *:");
		cvvLabel.addStyleName("label");
		paymentPanel.add(cvvLabel);
		TextBox cvvInput = new TextBox();
		paymentPanel.add(cvvInput);
		Label nationLabel = new Label("Nazione *:");
		nationLabel.addStyleName("label");
		paymentPanel.add(nationLabel);
		TextBox nationInput = new TextBox();
		paymentPanel.add(nationInput);
		Label capLabel = new Label("CAP *:");
		capLabel.addStyleName("label");
		paymentPanel.add(capLabel);
		TextBox capInput = new TextBox();
		paymentPanel.add(capInput);
		
		Button confirmSubscribed = new Button("Conferma iscrizione");
		confirmSubscribed.addStyleName("btn");
		paymentPanel.add(confirmSubscribed); 
		
		confirmSubscribed.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	MySessionServiceAsync sessionSvc = GWT.create(MySessionService.class);
            	UserServiceAsync userSvc = GWT.create(UserService.class);
            	
            	userSvc.getUserByNickname(getNickname(), new AsyncCallback<User>() {
        			@Override
        			public void onFailure(Throwable caught) {
        				Window.alert("Cannot load symbols: " + caught.getMessage());
        			}
        			@Override
        			public void onSuccess(User user) {
        				if(user != null && nccInput.getText().length() > 0 && cardHolderNameInput.getText().length() > 0 && cardHolderSurnameInput.getText().length() > 0 && cardDeadlineInput.getValue() != null && cvvInput.getText().length() > 0 && nationInput.getText().length() > 0 && capInput.getText().length() > 0) {
	        				userSvc.upgradeUser(user, nccInput.getText() , cardHolderNameInput.getText(), cardHolderSurnameInput.getText(), cardDeadlineInput.getValue(), cvvInput.getText(), nationInput.getText(), capInput.getText(), new AsyncCallback<User>() {
	                			@Override
	                			public void onFailure(Throwable caught) {
	                				Window.alert("Cannot load symbols: " + caught.getMessage());
	                			}
	                			@Override
	                			public void onSuccess(User user) {
	                				if(user != null) {
	        	        		        sessionSvc.exitSession( new AsyncCallback<Void>() {
	        	        		            @Override
	        	        		            public void onSuccess(Void result) {
	        	    	        		        sessionSvc.setSession( user, new AsyncCallback<Void>() {
	        	    	        		            @Override
	        	    	        		            public void onSuccess(Void result2) {
	        	    	        		            	
	        	    	        		            }
	        	    	        		            @Override
	        	    	        		            public void onFailure(Throwable caught) {
	        	    	        		                Window.alert(caught.getMessage());
	        	    	        		            }
	        	    	        		        });
	        	        		            }
	        	        		            @Override
	        	        		            public void onFailure(Throwable caught) {
	        	        		                Window.alert(caught.getMessage());
	        	        		            }
	        	        		        });
	        	        		        RootPanel.get("mainContent").clear();
	                					RootPanel.get("mainContent").add(user.getProfilePage());
	                				}else {
	                					Window.alert("L'upgrade del profilo non � andata a buon fine. Riprovare");
	                					RootPanel.get("mainContent").clear();
	                					RootPanel.get("mainContent").add(getProfilePage());
	                				}
	                			}
	                		});
        				} else {
        					Window.alert("Compila tutti i campi obbligatori");
        				}
        			}
        		});
            }
		});
		return paymentPanel;
	}
	
	public VerticalPanel getArticles(ArrayList<String> favouriteCategories) {
		VerticalPanel articlesPanel = new VerticalPanel();
		UserServiceAsync userSvc = GWT.create(UserService.class);
		
		userSvc.getArticlesForUser(favouriteCategories, new AsyncCallback<ArrayList<Article>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Cannot load symbols: " + caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<Article> result) {
				for(int i = result.size() - 1; i >= 0; i--) {
					articlesPanel.add(result.get(i).getArticlePreview(result.get(i)));
				}
			}
		});
		return articlesPanel;
	}
	
	public VerticalPanel getYourLikesAndComments(String nicknameUser) {
		VerticalPanel yourLikesAndComments = new VerticalPanel();
    	UserServiceAsync userSvc = GWT.create(UserService.class);
		userSvc.getArticlesLikedAndCommentedByUser(nicknameUser, new AsyncCallback<ArrayList<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Cannot load symbols: " + caught.getMessage());
			}
			@Override
			public void onSuccess(ArrayList<String> result) {
				for(int i = result.size()-1; i >=0 ; i--) {
		    		String [] split = result.get(i).split("&*&");
		    		Integer idArticle = Integer.valueOf(split[0]);
		    		String action = split[2];
		    		Hyperlink linkAction = new Hyperlink(action, "");
		    		linkAction.addClickHandler(new ClickHandler() {
		                public void onClick(ClickEvent event) {
		                	RootPanel.get("mainContent").clear();
		                	RootPanel.get("mainContent").add(getArticleById(idArticle));
		                }
		    		});
		    		linkAction.addStyleName("yourAction");
		    		yourLikesAndComments.add(linkAction);
		    		HTML html = new HTML("<hr  style=\"width:100%;\" />");
		    		yourLikesAndComments.add(html);
		    	}
			}
		});
		
		return yourLikesAndComments;
	}
	
	public VerticalPanel getArticleById(int idArticles) {
		RootPanel.get("mainContent").clear();
		VerticalPanel articlePanel = new VerticalPanel();
		Button buttonHomePage = new Button("Torna indietro");
		buttonHomePage.addStyleName("btn");
		
		buttonHomePage.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	RootPanel.get("mainContent").clear();
            	RootPanel.get("mainContent").add(getHomePage());
            }
		});
		
		UserServiceAsync userSvc = GWT.create(UserService.class);
    	userSvc.getArticleById(idArticles, new AsyncCallback<Article>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
			}
			@Override
			public void onSuccess(Article article) {	
				if(article == null) {
					RootPanel.get("mainContent").add(buttonHomePage);
					Label error = new Label("ARTICOLO CANCELLATO DALL'ADMIN!");
					error.addStyleName("label");
					RootPanel.get("mainContent").add(error);
				}else {
					articlePanel.add(article.getArticlePage(article));
				}
			}
		});
		
		return articlePanel;
	}
}
