package bolognino.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;

public class IlRestoDelBolognino implements EntryPoint {
	private UserServiceAsync userSvc = GWT.create(UserService.class);

	public void onModuleLoad() {
		userSvc.getHomePage( new AsyncCallback<HomePage>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Cannot load symbols: " + caught.getMessage());
			}
			@Override
			public void onSuccess(HomePage result) {
				MySessionServiceAsync sessionSvc = GWT.create(MySessionService.class);
				
		        sessionSvc.getSession( new AsyncCallback<User>() {

		            @Override
		            public void onSuccess(User user) {
		                // here is the result
		            	if(user != null) {
		            		RootPanel.get("mainContent").clear();
		            		RootPanel.get("mainContent").add(user.getProfilePage());
		            	}else {
		            		RootPanel.get("mainContent").clear();
	        				RootPanel.get("mainContent").add(result.getHomePage());
		            	}
		            }
		            @Override
		            public void onFailure(Throwable caught) {
		                Window.alert(caught.getMessage());
		            }
		        });
			}
		});
	}
}
