package bolognino.server;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;

import bolognino.client.MySessionService;
import bolognino.client.MySessionServiceAsync;
import bolognino.client.User;

public class MySessionServiceTest extends GWTTestCase {
	MySessionServiceAsync mySessionService;
	
	@Override
	public String getModuleName() {
		return "bolognino.test.Tests";
	}
	
	/*void setSession(User user, AsyncCallback<Void> callback);*/
	public void test1() {
		mySessionService = GWT.create(MySessionService.class);
		User user = new User("nickname", "password", "email", "firstname", "surname", new Date(), "state", "province", "region", "city", "address", true, "birthPlace", new ArrayList<String>(), new ArrayList<String>());
		
		mySessionService.setSession(user, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Void result) {
				assertTrue(true);
			}
		});
	}
    /*void exitSession(AsyncCallback<Void> callback);*/
	public void test2() {
		mySessionService = GWT.create(MySessionService.class);
		mySessionService.exitSession(new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Void result) {
				assertTrue(true);
			}
		});
	}
    /*void getSession(AsyncCallback<User> callback);*/
	public void test3() {
		mySessionService = GWT.create(MySessionService.class);
		mySessionService.getSession(new AsyncCallback<User>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(User user) {
				if(user instanceof User) {
					assertTrue(true);
				} else {
					assertFalse(false);
				}
			}
		});
	}
}
