package bolognino.server;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;

import bolognino.client.AccessRegistrationAuthorPage;
import bolognino.client.Article;
import bolognino.client.Author;
import bolognino.client.Category;
import bolognino.client.HomePage;
import bolognino.client.LoginPage;
import bolognino.client.RegistrationAuthorPage;
import bolognino.client.RegistrationUserPage;
import bolognino.client.Subscribed;
import bolognino.client.User;
import bolognino.client.UserService;
import bolognino.client.UserServiceAsync;

public class UserServiceImplTest extends GWTTestCase{
	
	UserServiceAsync userService;

	@Override
	public String getModuleName() {
		return "bolognino.test.Tests";
	}
	
	/*
	void getRegistrationUserPage(AsyncCallback<RegistrationUserPage> callback);
	*/
	public void test1() {
		userService = GWT.create(UserService.class);
		userService.getRegistrationUserPage(new AsyncCallback<RegistrationUserPage>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(RegistrationUserPage result) {
				if(result instanceof RegistrationUserPage) {
					assertTrue(true);
				}else {
					assertFalse(false);
				}
			}
		});
	}
	/*
	void getAccessRegistrationAuthorPage(AsyncCallback<AccessRegistrationAuthorPage> callback);
	*/
	public void test2() {
		userService = GWT.create(UserService.class);
		userService.getAccessRegistrationAuthorPage(new AsyncCallback<AccessRegistrationAuthorPage>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(AccessRegistrationAuthorPage result) {
				if(result instanceof AccessRegistrationAuthorPage) {
					assertTrue(true);
				}else {
					assertFalse(false);
				}
			}
		});
	}
	/*
	void getRegistrationAuthorPage(AsyncCallback<RegistrationAuthorPage> callback); */
	public void test3() {
		userService = GWT.create(UserService.class);
		userService.getRegistrationAuthorPage(new AsyncCallback<RegistrationAuthorPage>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(RegistrationAuthorPage result) {
				if(result instanceof RegistrationAuthorPage) {
					assertTrue(true);
				}else {
					assertFalse(false);
				}
			}
		});
	}
	/*
	void getHomePage(AsyncCallback<HomePage> callback);*/
	public void test4() {
		userService = GWT.create(UserService.class);
		userService.getHomePage(new AsyncCallback<HomePage>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(HomePage result) {
				if(result instanceof HomePage) {
					assertTrue(true);
				}else {
					assertFalse(false);
				}
			}
		});
	}
	/*
	void getLoginPage(AsyncCallback<LoginPage> callback);*/
	public void test5() {
		userService = GWT.create(UserService.class);
		userService.getLoginPage(new AsyncCallback<LoginPage>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(LoginPage result) {
				if(result instanceof LoginPage) {
					assertTrue(true);
				}else {
					assertFalse(false);
				}
			}
		});
	}
	/*
	void newUser(String nickname, String password, String email, String firstname, String surname, Date birthday, String state, String province, String region, String city, String address, Boolean gender, String birthPlace, ArrayList<String> categories, ArrayList<Article> likesAndComments, AsyncCallback<Boolean> callback);
	*/
	public void test6() {
		userService = GWT.create(UserService.class);
		//nickname, password, email, firstname, surname, Date birthday, state, province, region, city, address, Boolean gender, birthPlace, ArrayList<String> categories, ArrayList<Article> likesAndComments
		userService.newUser("nickname","password","email@email.email","firstname","surname",new Date(),"state","province","region","city","address",true,"birthPlace",new ArrayList<String>(),new ArrayList<Article>(), new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Boolean result) {
				if(result) {
					assertTrue(result);
				} else {
					assertFalse(result);
				}
			}
		});
	}
	/*
	void newAuthor(String nickname, String password, String email, String firstname, String surname, Date birthday, String state, String province, String region, String city, String address, Boolean gender, String birthPlace, ArrayList<String> categories, ArrayList<Article> likesAndComments, AsyncCallback<Boolean> callback);*/
	public void test7() {
		userService = GWT.create(UserService.class);
		//nickname, password, email, firstname, surname, Date birthday, state, province, region, city, address, Boolean gender, birthPlace, ArrayList<String> categories, ArrayList<Article> likesAndComments
		userService.newAuthor("nickname1","password1","email1@email.email","firstname1","surname1",new Date(),"state1","province1","region1","city1","address1",true,"birthPlace1",new ArrayList<String>(),new ArrayList<Article>(), new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Boolean result) {
				if(result) {
					assertTrue(result);
				} else {
					assertFalse(result);
				}
			}
		});
	}
	/*
	void upgradeUser(User user, String ncc, String cardHolderName, String cardHolderSurname, Date cardDeadline, String cvv, String nation, String cap, AsyncCallback<User> callback);
	void getUserByNickname(String nicknameUser, AsyncCallback<User> callback);*/
	public void test8() {
		userService = GWT.create(UserService.class);
		//User user, ncc, cardHolderName, cardHolderSurname, Date cardDeadline, cvv, nation, cap
		userService.getUserByNickname("nickname", new AsyncCallback<User>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(User user) {
				if(user instanceof User) {
					userService.upgradeUser(user, "ncc", "cardHolderName", "cardHolderSurname", new Date(), "cvv", "nation", "cap", new AsyncCallback<User>() {
						@Override
						public void onFailure(Throwable caught) {
							fail("Impossible");
						}
						@Override
						public void onSuccess(User user) {
							if(user instanceof Subscribed) {
								assertTrue(true);
							} else {
								assertFalse(false);
							}
						}
					});
				} else {
					assertFalse(false);
				}
			}
		});			
	}
	/*
	void downgradeUser(User user, AsyncCallback<User> callback);
	void getUserByNickname(String nicknameUser, AsyncCallback<User> callback);*/
	public void test9() {
		userService = GWT.create(UserService.class);
		//User user
		userService.getUserByNickname("nickname", new AsyncCallback<User>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(User user) {
				if(user instanceof Subscribed) {
					userService.downgradeUser(user, new AsyncCallback<User>() {
						@Override
						public void onFailure(Throwable caught) {
							fail("Impossible");
						}
						@Override
						public void onSuccess(User user) {
							if(user instanceof User) {
								assertTrue(true);
							} else {
								assertFalse(false);
							}
						}
					});
				} else {
					assertFalse(false);
				}
			}
		});			
	}
	/*
	void getAuthentication(String nickname, String password, AsyncCallback<User> callback);*/
	public void test10() {
		userService = GWT.create(UserService.class);
		//User user
		userService.getAuthentication("nickame", "password", new AsyncCallback<User>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(User user) {
				if(user instanceof User || user instanceof Subscribed || user instanceof Author) {
					assertTrue(true);
				} else {
					assertFalse(false);
				}
			}
		});			
	}
	/*
	void checkAuthenticationToken(String token, AsyncCallback<Boolean> callback);*/
	public void test11() {
		userService = GWT.create(UserService.class);
		userService.checkAuthenticationToken("123STELLA", new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Boolean result) {
				if(result) {
					assertTrue(result);
				}else {
					assertFalse(result);
				}
			}
		});
	}
	/*
	void getCategoriesArray(AsyncCallback<String[]> callback);*/
	public void test12() {
		userService = GWT.create(UserService.class);
		userService.getCategoriesArray(new AsyncCallback<String[]>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(String[] result) {
				//Ci sono almeno quelle di default o almeno 1 SEMPRE!
				if(result.length >= 0) {
					assertTrue(true);
				} else {
					assertFalse(false);
				}
			}
		});
	}
	/*
	void getCategoriesArrayList(AsyncCallback<ArrayList<Category>> callback);*/
	public void test13() {
		userService = GWT.create(UserService.class);
		userService.getCategoriesArrayList(new AsyncCallback<ArrayList<Category>>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(ArrayList<Category> result) {
				if(result instanceof ArrayList) {
					assertTrue(true);
				} else {
					assertFalse(false);
				}
			}
		});
	}
	/*
	void getArticlesForUser(ArrayList<String> subCategories, AsyncCallback<ArrayList<Article>> callback);*/
	public void test14() {
		userService = GWT.create(UserService.class);
		userService.getArticlesForUser(new ArrayList<String>(), new AsyncCallback<ArrayList<Article>>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(ArrayList<Article> result) {
				if(result.size() > 0) {
					for(int i = 0; i < result.size(); i++) {
						if(result.get(i) instanceof Article) {
							assertTrue(true);
						} else {
							assertFalse(false);
						}
					}
				} else {
					assertFalse(false);
				}				
			}
		});
	}
	/*
	void postComment(Article article, String userNickname, String commentText, AsyncCallback<Boolean> callback);*/
	public void test15() {
		userService = GWT.create(UserService.class);
		userService.getArticleById(1, new AsyncCallback<Article>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Article result) {
				if(result.getTitle() != null) {
					userService.postComment(result, "nickname", "commento di prova", new AsyncCallback<Boolean>() {
						@Override
						public void onFailure(Throwable caught) {
							fail("Impossible");
						}
						@Override
						public void onSuccess(Boolean result) {
							if(result) {
								assertTrue(result);
							} else {
								assertFalse(result);
							}
						}
					});
				}
			}
		});
	}
	/*void likeArticle(String nicknameUser, Article article, AsyncCallback<Boolean> callback);
	  void getArticleById(int idArticles, AsyncCallback<Article> callback);*/
	public void test16() {
		userService = GWT.create(UserService.class);
		userService.getArticleById(1, new AsyncCallback<Article>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Article result) {
				if(result.getTitle() != null) {
					userService.likeArticle("nickname", result, new AsyncCallback<Boolean>() {
						@Override
						public void onFailure(Throwable caught) {
							fail("Impossible");
						}
						@Override
						public void onSuccess(Boolean result) {
							if(result) {
								//assertEquals((Boolean) true, result);
								assertTrue(result);
							} else {
								assertFalse(result);
							}
						}
					});
				}
			}
		});
	}
	/*
	void getArticlesLikedAndCommentedByUser(String nicknameUser, AsyncCallback<ArrayList<String>> callback);*/
	public void test17() {
		userService = GWT.create(UserService.class);
		userService.getArticlesLikedAndCommentedByUser("nickname", new AsyncCallback<ArrayList<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(ArrayList<String> result) {
				for(int i = 0; i < result.size(); i++) {
					if(result.get(i).length() > 0) {
						assertTrue(true);
					} else {
						assertFalse(false);
					}
				}
			}
		});
	}
}
