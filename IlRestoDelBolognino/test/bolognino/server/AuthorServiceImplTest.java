package bolognino.server;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;

import bolognino.client.AccessRegistrationAuthorPage;
import bolognino.client.Article;
import bolognino.client.Author;
import bolognino.client.AuthorService;
import bolognino.client.AuthorServiceAsync;
import bolognino.client.Category;
import bolognino.client.HomePage;
import bolognino.client.LoginPage;
import bolognino.client.RegistrationAuthorPage;
import bolognino.client.RegistrationUserPage;
import bolognino.client.Subscribed;
import bolognino.client.User;
import bolognino.client.UserService;
import bolognino.client.UserServiceAsync;

public class AuthorServiceImplTest extends GWTTestCase{
	
	AuthorServiceAsync authorService;

	@Override
	public String getModuleName() {
		return "bolognino.test.Tests";
	}

	/*void newArticle(Author author, String title, String content, String category, String subCategory, AsyncCallback<Boolean> callback);*/
	public void test1() {
		authorService = GWT.create(AuthorService.class);
		Author author = new Author("nickname", "password", "email", "firstname", "surname", new Date(), "state", "province", "region", "city", "address", true, "birthPlace", new ArrayList<String>(), new ArrayList<String>());

		authorService.newArticle(author, "title", "content", "category", "subCategory", new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Boolean result) {
				if(result) {
					assertTrue(true);
				}else {
					assertFalse(false);
				}
			}
		});
	}
	/*void newArticlePremium(Author author, String title, String content, String category, String subCategory, AsyncCallback<Boolean> callback);*/
	public void test2() {
		authorService = GWT.create(AuthorService.class);
		Author author = new Author("nickname", "password", "email", "firstname", "surname", new Date(), "state", "province", "region", "city", "address", true, "birthPlace", new ArrayList<String>(), new ArrayList<String>());

		authorService.newArticlePremium(author, "title", "content", "category", "subCategory", new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Boolean result) {
				if(result) {
					assertTrue(true);
				}else {
					assertFalse(false);
				}
			}
		});
	}
	/*void getArticlesByAuthor(String nicknameAuthor, AsyncCallback<ArrayList<Article>> callback);*/
	public void test3() {
		authorService = GWT.create(AuthorService.class);
		authorService.getArticlesByAuthor("scrittore", new AsyncCallback<ArrayList<Article>>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(ArrayList<Article> result) {
				if(result instanceof ArrayList) {
					assertTrue(true);
				}else {
					assertFalse(false);
				}
			}
		});
	}
	
	/*void getArticleById(int idArticles, AsyncCallback<Article> callback);*/
	public void test4() {
		authorService = GWT.create(AuthorService.class);
		authorService.getArticleById(1, new AsyncCallback<Article>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Article article) {
				if(article.getTitle() != null) {
					assertTrue(true);
				} else {
					assertFalse(false);
				}
			}
		});
	}
	/*void editArticle(Article article, AsyncCallback<Boolean> callback);*/
	public void test5() {
		authorService = GWT.create(AuthorService.class);
		authorService.getArticleById(1, new AsyncCallback<Article>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Article article) {
				if(article.getTitle() != null) {
					authorService.editArticle(article, new AsyncCallback<Boolean>() {
						@Override
						public void onFailure(Throwable caught) {
							fail("Impossible");
						}
						@Override
						public void onSuccess(Boolean result) {
							if(result) {
								assertTrue(true);
							} else {
								assertFalse(false);
							}
						}
					});
				} else {
					assertFalse(false);
				}
			}
		});
	}
	
	/*void deleteArticle(int idArticle, AsyncCallback<Boolean> callback);
	*/
	public void test6() {
		authorService = GWT.create(AuthorService.class);
		authorService.deleteArticle(1, new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Boolean result) {
				if(result) {
					assertTrue(true);
				} else {
					assertFalse(false);
				}
			}
		});
	}
}
