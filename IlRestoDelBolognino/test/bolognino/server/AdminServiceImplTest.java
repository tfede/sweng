package bolognino.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;

import bolognino.client.AdminService;
import bolognino.client.AdminServiceAsync;
import bolognino.client.Article;
import bolognino.client.Author;
import bolognino.client.AuthorService;
import bolognino.client.Category;
import bolognino.client.User;

public class AdminServiceImplTest extends GWTTestCase {
	AdminServiceAsync adminService;
	
	@Override
	public String getModuleName() {
		return "bolognino.test.Tests";
	}
	
	/*
	void replaceCategories(Map<String,ArrayList<String>> newCategories, AsyncCallback<Boolean> callback);*/
	public void test1() {
		adminService = GWT.create(AdminService.class);
		Map <String,ArrayList<String>> map = new HashMap<String,ArrayList<String>>();
		adminService.replaceCategories(map, new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Boolean result) {
				if(result) {
					assertTrue(true);
				}else {
					assertFalse(false);
				}
			}
		});
	}
	/*void getCategoriesList(AsyncCallback<ArrayList<Category>> callback);*/
	public void test2() {
		adminService = GWT.create(AdminService.class);
		adminService.getCategoriesList(new AsyncCallback<ArrayList<Category>>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(ArrayList<Category> result) {
				if(result instanceof ArrayList) {
					assertTrue(true);
				} else {
					assertFalse(false);
				}
			}
		});
	}
	/*void getArticlesWithCommentsToModerate(AsyncCallback<ArrayList<Article>> callback);*/
	public void test3() {
		adminService = GWT.create(AdminService.class);
		adminService.getArticlesWithCommentsToModerate(new AsyncCallback<ArrayList<Article>>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(ArrayList<Article> result) {
				if(result instanceof ArrayList) {
					assertTrue(true);
				} else {
					assertFalse(false);
				}
			}
		});
	}
	/*void getAllArticles(AsyncCallback<ArrayList<Article>> callback);*/
	public void test4() {
		adminService = GWT.create(AdminService.class);
		adminService.getAllArticles(new AsyncCallback<ArrayList<Article>>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(ArrayList<Article> result) {
				if(result instanceof ArrayList) {
					assertTrue(true);
				} else {
					assertFalse(false);
				}
			}
		});
	}
	/*void approveComment(int idArticle, int idComment, AsyncCallback<Boolean > callback);*/
	public void test5() {
		adminService = GWT.create(AdminService.class);
		adminService.approveComment(1, 1, new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Boolean result) {
				if(result) {
					assertTrue(true);
				} else {
					assertFalse(false);
				}
			}
		});
	}
	/*void declineComment(int idArticle, int idComment, AsyncCallback<Boolean > callback);*/
	public void test6() {
		adminService = GWT.create(AdminService.class);
		adminService.declineComment(1, 1, new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Boolean result) {
				if(result) {
					assertTrue(true);
				} else {
					assertFalse(false);
				}
			}
		});
	}
	/*void getAuthorRegistred(AsyncCallback<ArrayList<User>> callback);*/
	public void test7() {
		adminService = GWT.create(AdminService.class);
		adminService.getAuthorRegistred(new AsyncCallback<ArrayList<User>>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(ArrayList<User> result) {
				if(result instanceof ArrayList) {
					assertTrue(true);
				} else {
					assertFalse(false);
				}
			}
		});
	}
	/*void replaceArticles(Map<Integer, Article> articles, AsyncCallback<Boolean> callback);*/
	public void test8() {
		adminService = GWT.create(AdminService.class);
		Map <Integer,Article> map = new HashMap<Integer,Article>();
		adminService.replaceArticles(map, new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Boolean result) {
				if(result) {
					assertTrue(true);
				} else {
					assertFalse(false);
				}
			}
		});
	}
	/*void removeComment(int idArticle, int idComment, AsyncCallback<Boolean> callback);*/
	public void test9() {
		adminService = GWT.create(AdminService.class);
		adminService.removeComment(1, 1, new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Impossible");
			}
			@Override
			public void onSuccess(Boolean result) {
				if(result) {
					assertTrue(true);
				} else {
					assertFalse(false);
				}
			}
		});
	}
}
